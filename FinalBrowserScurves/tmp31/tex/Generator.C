// pdflatex -shell-escape Scurves.tex


Int_t GetnPMTFromXY(Int_t x, Int_t y);





void Generator(void)
{
	FILE *fd = fopen("Scurves.tex", "w");



	fprintf(fd, "\\documentclass[12pt,a4paper]{article}\n");
	fprintf(fd, "\\usepackage[T1]{fontenc}\n");
	fprintf(fd, "\\usepackage[utf8x]{inputenc}\n");
	fprintf(fd, "\\usepackage{hyperref}\n");
	fprintf(fd, "\\usepackage{url}\n");
	fprintf(fd, "\\usepackage{graphicx}\n");
	fprintf(fd, "\\usepackage{subfigure}\n");
	fprintf(fd, "\\bibliographystyle{unsrt}\n");
	fprintf(fd, "\\renewcommand{\\thesection}{\\arabic{section}.}\n");
	fprintf(fd, "\\begin{document}\n");
	fprintf(fd, "\\begin{center}\n");
	fprintf(fd, "\\huge\n");
	fprintf(fd, "\\bf\n");
	fprintf(fd, "\\large\n");
	fprintf(fd, "\\vspace{1cm}\n");
	fprintf(fd, "\\end{center}\n");
	fprintf(fd, "\\section{DAC10: Lech 14-08-2019  Data: 1100V cathode2 13-08-2019}\n");





	for(Int_t y=0; y<48; y++)
	{
		fprintf(fd, "\\clearpage\n");
	for(Int_t x=0; x<48; x+=2)
	{


	fprintf(fd, "\\begin{figure}[ht]\n");
	fprintf(fd, "\\centering\n");
	fprintf(fd, "\\mbox{\n");
	fprintf(fd, "\\subfigure{\\includegraphics[width=0.49\\textwidth]{../EPS/X_%02d__Y_%02d__PMT__%02d.eps}}\\quad\n",x,y, GetnPMTFromXY(x, y));
	fprintf(fd, "\\subfigure{\\includegraphics[width=0.49\\textwidth]{../EPS/X_%02d__Y_%02d__PMT__%02d.eps}}\n",x+1, y, GetnPMTFromXY(x+1, y));
	fprintf(fd, "}\n");
	fprintf(fd, "\\caption{PMT: %02d}\n", GetnPMTFromXY(x, y));
	fprintf(fd, "\\end{figure}\n\n\n");
	//fprintf(fd, "\\clearpage\n");


	}
	}

	fprintf(fd, "\\end{document}\n");




	fclose(fd);


}


Int_t GetnPMTFromXY(Int_t x, Int_t y)
{
        return (5-(y/8))*6 +(x/8)+ 1;
}

