#include <TGFrame.h>
#include <TGLabel.h>
#include <TGListBox.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TFile.h>
#include <TH1F.h>
#include <TText.h>
#include <TList.h>
#include <TLatex.h>
#include <TH2F.h>

#include <Riostream.h>


#include <TColor.h>
#include <TROOT.h>
#include <TGClient.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include "Grafy.h"
//#include "SRC/TAmplituda.h"
//#include "SRC/TSplotX.h"
//#include "SRC/TPrzekroje.h"

class MyMainFrame : public TGMainFrame {
	
	private:
	TGMainFrame *fMainFrame;		// Glowna ramka (zawsze)
	TGHorizontalFrame *fHFrameCanvas;		// Pionowa ramka dla zelaza
	TGHorizontalFrame *fHFrameButtons;	// Pionowa ramka dla protonu
	
	TGGroupFrame *fGFrameCanvas, *fGFrameButtons, *fGFrameData;	// Pionowe ramki (z obramowaniem i podpisem, grupowe)

	TRootEmbeddedCanvas  *fEcan;	// Canvas zalaczony w oknie
	//TRootEmbeddedCanvas  *fEcanChi2;


//	TGGroupFrame *fGM, *fGK, *fGA, *fGR, *fGL, *fGD;	// Ramki pol zmiany parametrow
	TGGroupFrame *fGD;

//	TGTextButton *fMPlus, *fMMinus;
//	TGTextButton *fKPlus, *fKMinus;
//	TGTextButton *fRPlus, *fRMinus;		//	Theta
//	TGTextButton *fAPlus, *fAMinus;		//	Azimuth
//	TGTextButton *fLPlus, *fLMinus;		//	GTU
	TGTextButton *fDPlus, *fDMinus;

	TGTextButton *fSzukaj;

//	TGLabel *fLM, *fLK, *fLA, *fLR, *fLL, *fLD;
	TGLabel *fLD;


//	Double_t M, K, A, R, L;		// Zmienne w ktorych przechowywane sa aktualne wartosci	
//	Int_t D;
	
//	TGraph *grOmega;

//	TGraph *grChi2;
//	TLatex *latChi2;	
//	Double_t ChiSquareMin;




//	TGraphErrors *Data;

	
	public:
	MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
	virtual ~MyMainFrame();
	void DoExit();
	Char_t * GetCanvasName(Double_t tt, Double_t aa, Double_t gg);
//	TGraph * ChiSquare(TGraphErrors *dane, TGraph *suma,  Double_t  &ChOut);


//	TLatex *Chi2Lat(TGraph *gr);
	//void SzukajMinimum(void);


/*	
	void MPlus();
        void MMinus();

	void KPlus();
	void KMinus();

	void RPlus();
        void RMinus();

	void APlus();
        void AMinus();

	void LPlus();
        void LMinus();
*/

	void DPlus();
	void DMinus();


	void DoDraw();


	ClassDef(MyMainFrame,0);
};


MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h) : TGMainFrame(p, w, h)
{

/*
*/
//	 M= 0.703695; R= 0.924435; K= 0.450535*8.18; A= 0.015122*8.18;

//	Data = F546();

	//ff = new TFile("../../XY01.root", "read");

	// Main Frame
	fMainFrame = new TGMainFrame(0, 10, 10, kMainFrame | kVerticalFrame);
	fMainFrame->SetLayoutBroken(kTRUE);


	//__________________________________________________________________________	
	// CANVAS
	fHFrameCanvas = new TGHorizontalFrame(fMainFrame, 375, 380, kHorizontalFrame);
	fGFrameCanvas = new TGGroupFrame(fHFrameCanvas, "CANVAS");
	fGFrameCanvas->SetLayoutBroken(kTRUE);

	//____________________________________________
	// TUTAJ CIALO PROGRAMU DLA CANVAS
	fEcan = new TRootEmbeddedCanvas(0,fGFrameCanvas,500,300);
   	fGFrameCanvas->AddFrame(fEcan, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                     kLHintsExpandX  | kLHintsExpandY,0,0,1,1));
	fEcan->MoveResize(10,20,700,600);

	//fEcanChi2 = new TRootEmbeddedCanvas(0,fGFrameCanvas,500,300);
        //fGFrameCanvas->AddFrame(fEcanChi2, new TGLayoutHints(kLHintsTop | kLHintsLeft |
        //                             kLHintsExpandX  | kLHintsExpandY,0,0,1,1));
        //fEcanChi2->MoveResize(10,430,700,200);


	//_______________________________________________________________________

	fHFrameCanvas->AddFrame(fGFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fGFrameCanvas->MoveResize(5,5, 720, 640);
	fMainFrame->AddFrame(fHFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fHFrameCanvas->MoveResize(10,10,730 ,650);
	// END OF CANVAS
	//_____________________________________________________________________________________



	//___________________________________________________________________________
	//BUTTONS
	fHFrameButtons = new TGHorizontalFrame(fMainFrame, 175, 380, kHorizontalFrame);
        fGFrameButtons = new TGGroupFrame(fHFrameButtons, "PARAMETERS");
        fGFrameButtons->SetLayoutBroken(kTRUE);

	//________________________________________________ 
	// TUTAJ CIALO PROGRAMU DLA BUTTONS

/*
	// **************** M ***************************
        fGM = new TGGroupFrame(fGFrameButtons, "Mh:");
        fGM->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- M ---------------------
        fMPlus = new TGTextButton(fGM, "&PLUS");
        fMPlus->Connect("Pressed()", "MyMainFrame", this, "MPlus()");
        fGM->AddFrame(fMPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fMPlus->MoveResize(10, 15, 60,30);

        fMMinus = new TGTextButton(fGM, "&MINUS");
        fMMinus->Connect("Pressed()", "MyMainFrame", this, "MMinus()");
        fGM->AddFrame(fMMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fMMinus->MoveResize(80, 15, 60,30);
        // ----------------------------------------------------------------

	fLM = new TGLabel(fGM, "0.73 ");
	fGM->AddFrame(fLM, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fLM->MoveResize(10, 55, 140, 20);


        fGFrameButtons->AddFrame(fGM, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGM->MoveResize(5,15,155, 90);
        // *********************
*/


/*	
	// **************** K ***************************
        fGK = new TGGroupFrame(fGFrameButtons, "Kb:");
        fGK->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- K ---------------------
        fKPlus = new TGTextButton(fGK, "&PLUS");
        fKPlus->Connect("Pressed()", "MyMainFrame", this, "KPlus()");
        fGK->AddFrame(fKPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fKPlus->MoveResize(10, 15, 60,30);

        fKMinus = new TGTextButton(fGK, "&MINUS");
        fKMinus->Connect("Pressed()", "MyMainFrame", this, "KMinus()");
        fGK->AddFrame(fKMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fKMinus->MoveResize(80, 15, 60,30);

	fLK = new TGLabel(fGK, "5.04");
        fGK->AddFrame(fLK, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLK->MoveResize(10, 55, 140, 20);


        // ----------------------------------------------------------------

        fGFrameButtons->AddFrame(fGK, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGK->MoveResize(5,105,155, 90);
        // *********************





	// **************** R ***************************
        fGR = new TGGroupFrame(fGFrameButtons, "Radius:");
        fGR->SetLayoutBroken(kTRUE);

	// --- Przyciski +/- R ---------------------
	fRPlus = new TGTextButton(fGR, "&PLUS");
        fRPlus->Connect("Pressed()", "MyMainFrame", this, "RPlus()");
        fGR->AddFrame(fRPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fRPlus->MoveResize(10, 15, 60,30);

        fRMinus = new TGTextButton(fGR, "&MINUS");
        fRMinus->Connect("Pressed()", "MyMainFrame", this, "RMinus()");
        fGR->AddFrame(fRMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fRMinus->MoveResize(80, 15, 60,30);

	fLR = new TGLabel(fGR, "0.28");
        fGR->AddFrame(fLR, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLR->MoveResize(10, 55, 140, 20);


	// ----------------------------------------------------------------

        fGFrameButtons->AddFrame(fGR, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGR->MoveResize(5,195,155, 90);
        // *********************

	// **************** A ***************************
        fGA = new TGGroupFrame(fGFrameButtons, "Height:");
        fGA->SetLayoutBroken(kTRUE);

	// --- Przyciski +/- A ---------------------
	fAPlus = new TGTextButton(fGA, "&PLUS");
        fAPlus->Connect("Pressed()", "MyMainFrame", this, "APlus()");
        fGA->AddFrame(fAPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fAPlus->MoveResize(10, 15, 60,30);

        fAMinus = new TGTextButton(fGA, "&MINUS");
        fAMinus->Connect("Pressed()", "MyMainFrame", this, "AMinus()");
        fGA->AddFrame(fAMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fAMinus->MoveResize(80, 15, 60,30);

	fLA = new TGLabel(fGA, "4.1");
        fGA->AddFrame(fLA, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLA->MoveResize(10, 55, 140, 20);
	// ------------------------------------------------------------


        fGFrameButtons->AddFrame(fGA, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGA->MoveResize(5,285,155, 90);
        // *********************

	// **************** L ***************************
        fGL = new TGGroupFrame(fGFrameButtons, "Lambda:");
        fGL->SetLayoutBroken(kTRUE);

	// --- Przyciski +/- L ---------------------
	fLPlus = new TGTextButton(fGL, "&PLUS");
        fLPlus->Connect("Pressed()", "MyMainFrame", this, "LPlus()");
        fGL->AddFrame(fLPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLPlus->MoveResize(10, 15, 60,30);

        fLMinus = new TGTextButton(fGL, "&MINUS");
        fLMinus->Connect("Pressed()", "MyMainFrame", this, "LMinus()");
        fGL->AddFrame(fLMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLMinus->MoveResize(80, 15, 60,30);

	fLL = new TGLabel(fGL, "0.19");
        fGL->AddFrame(fLL, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLL->MoveResize(10, 55, 140, 20);

	// -----------------------------------------------------------------


        fGFrameButtons->AddFrame(fGL, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGL->MoveResize(5,375,155, 90);
        // *********************
*/

	// **************** D ***************************
        fGD = new TGGroupFrame(fGFrameButtons, "DATA:");
        fGD->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- D 
        fDPlus = new TGTextButton(fGD, "&PLUS");
        fDPlus->Connect("Pressed()", "MyMainFrame", this, "DPlus()");
        fGD->AddFrame(fDPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDPlus->MoveResize(10, 15, 60,30);

        fDMinus = new TGTextButton(fGD, "&MINUS");
        fDMinus->Connect("Pressed()", "MyMainFrame", this, "DMinus()");
        fGD->AddFrame(fDMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDMinus->MoveResize(80, 15, 60,30);
        // -----------------------------------------------------------------

	fGD->SetBackgroundColor(gROOT->GetColor(kGray)->GetPixel());
	fLD = new TGLabel(fGD, "1960 GeV");
        fGD->AddFrame(fLD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLD->MoveResize(10, 55, 140, 20);


	fSzukaj = new TGTextButton(fGD, "&DoDraw");
	fSzukaj->Connect("Pressed()", "MyMainFrame", this, "DoDraw()");
	fGD->AddFrame(fSzukaj, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fSzukaj->MoveResize(10, 85, 140, 20);


        fGFrameButtons->AddFrame(fGD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGD->MoveResize(5,15,155, 120);
        // *********************


	//________________________________________________________________________________________

        fHFrameButtons->AddFrame(fGFrameButtons, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGFrameButtons->MoveResize(5,15, 165, 600);


	//fGFrameButtons = new TGGroupFrame(fHFrameButtons, "PARAMETERS");
        //fGFrameButtons->SetLayoutBroken(kTRUE);

/*
	 // **************** D ***************************
        fGD = new TGGroupFrame(fHFrameButtons, "DATA:");
        fGD->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- D 
        fDPlus = new TGTextButton(fGD, "&PLUS");
        fDPlus->Connect("Pressed()", "MyMainFrame", this, "DPlus()");
        fGD->AddFrame(fDPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDPlus->MoveResize(10, 20, 60,30);

        fDMinus = new TGTextButton(fGD, "&MINUS");
        fDMinus->Connect("Pressed()", "MyMainFrame", this, "DMinus()");
        fGD->AddFrame(fDMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDMinus->MoveResize(80, 20, 60,30);
        // -----------------------------------------------------------------


        fHFrameButtons->AddFrame(fGD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGD->MoveResize(5,400,155, 30);
        // *********************
*/


        fMainFrame->AddFrame(fHFrameButtons, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fHFrameButtons->MoveResize(740,10,200 ,640);	
	//END OF BUTTONS
	//___________________________________________________________________________________________


	// Configuration of main frame
	fMainFrame->SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
	fMainFrame->MapSubwindows();
	fMainFrame->SetWindowName("Moje Nowe Okno");
	fMainFrame->Resize(fMainFrame->GetDefaultSize());
	fMainFrame->MapWindow();
	fMainFrame->Resize(w, h);
	fMainFrame->Layout();
	
	//for(Int_t i=0; i<300; i++)
	//SzukajMinimum();
	
}	

MyMainFrame::~MyMainFrame()
{
	Cleanup();
}


void MyMainFrame::DoExit()
{
	gApplication->Terminate(0);
}



// Funkcje zmieniajace dany parametr o odpowiadajacy mu skok w przod lub w tyl
//void MyMainFrame::XPlus()
//{	
//	if(X==1e7) X = 1e7; else
//	X = X + 2e6;
//	DoDraw();
//}

//void MyMainFrame::XMinus()
//{
//	if(X==-1e7) X = -1e7; else
//        X = X - 2e6;
//	DoDraw();       
//}

//void MyMainFrame::YPlus()
//{
//	if(Y==1e7) Y = 1e7; else
//        Y = Y + 2e6;
//	DoDraw();
//}

//void MyMainFrame::YMinus()
//{
//        if(Y==-1e7) Y = -1e7; else
//        Y = Y - 2e6;
//	DoDraw(); 
//}

/*
void MyMainFrame::MPlus()
{
        //if(M>=9.0) M = 9.0; else
        M = M + 0.01;
	fLM->SetText(Form("%.03f", M));
	fLM->Layout();
        DoDraw();
}

void MyMainFrame::MMinus()
{
        if(M<=0.2) M = 0.2; else
        M = M - 0.01;
	fLM->SetText(Form("%.03f", M));
        fLM->Layout();
        DoDraw();
}





void MyMainFrame::KPlus()
{
        //if(K>=10.0) K = 10.0; else
        K = K + 0.1;
	fLK->SetText(Form("%.02f", K));
        fLK->Layout();
        DoDraw();
}

void MyMainFrame::KMinus()
{
        if(K<=0.0) K = 0.0; else
        K = K - 0.1;
	fLK->SetText(Form("%.02f", K));
        fLK->Layout();
        DoDraw();
}





void MyMainFrame::RPlus()
{
        if(R>=20.0) R = 20.0; else
        R = R + 0.01;
	fLR->SetText(Form("%.02f", R));
        fLR->Layout();
        DoDraw();
}

void MyMainFrame::RMinus()
{
        if(R<=0.0) R = 0.0; else
        R = R - 0.01;
	fLR->SetText(Form("%.02f", R));
        fLR->Layout();
        DoDraw();
}

void MyMainFrame::APlus()
{
        //if(A>=20.0) A = 20.0; else
        A = A + 0.1;
	fLA->SetText(Form("%02.02f", A));
        fLA->Layout();
        DoDraw();
}

void MyMainFrame::AMinus()
{
        if(A<=0.0) A = 0.0; else
        A = A - 0.1;
	fLA->SetText(Form("%02.02f", A));
        fLA->Layout();
        DoDraw();
}


void MyMainFrame::LPlus()
{
        if(L>=0.30) L = 0.30; else
        L = L + 0.005;
	fLL->SetText(Form("%.03f", L));
        fLL->Layout();
        DoDraw();
}

void MyMainFrame::LMinus()
{
        if(L<=0.0) L = 0.0; else
        L = L - 0.005;
	fLL->SetText(Form("%.03f", L));
        fLL->Layout();
        DoDraw();
}
// koniec funkcji zmieniajacych parametry
*/

void MyMainFrame::DPlus()
{

/*
        if(D>=12) D = 12; else
        D++;
	switch(D){
	case 0:
                Data = F9_776();
                fLD->SetText("9.776 GeV");
                fLD->Layout();
                printf("DATA 0\n");
                break;
        case 1:
                Data = F13_761();
                fLD->SetText("13.761 GeV");
                fLD->Layout();
                printf("DATA 1\n");
                break;
        case 2:
                Data = F19_416();
                fLD->SetText("19.416 GeV");
                fLD->Layout();
                printf("DATA 2\n");
                break;
        case 3:
                Data = F23_46();
                fLD->SetText("23.46 GeV");
                fLD->Layout();
                printf("DATA 3\n");
                break;
	case 4:
                Data = F30_54();
                fLD->SetText("30.54 GeV");
                fLD->Layout();
                printf("DATA 4\n");
                break;
        case 5:
                Data = F44_64();
                fLD->SetText("44.64 GeV");
                fLD->Layout();
                printf("DATA 5\n");
                break;
        case 6:
                Data = F52_81();
                fLD->SetText("52.81 GeV");
                fLD->Layout();
                printf("DATA 6\n");
                break;
        case 7:
                Data = F62_07();
                fLD->SetText("62.07 GeV");
                fLD->Layout();
                printf("DATA 7\n");
                break;
	case 8:
                Data = F546();
                fLD->SetText("546 GeV");
                fLD->Layout();
                printf("DATA 8\n");
                break;
        case 9:
                //Data = F630();
                Data = F630();
                fLD->SetText("630 GeV");
                fLD->Layout();
                printf("DATA 9\n");
                break;
        case 10:
                Data = F1800();
                fLD->SetText("1800 GeV");
                fLD->Layout();
                printf("DATA 10\n");
                break;
        case 11:
                Data = F1960();
                fLD->SetText("1960 GeV");
                fLD->Layout();
                printf("DATA 11\n");
                break;
	case 12:
                Data = F7000();
                //Data = F1960();
                fLD->SetText("7000 GeV");
                fLD->Layout();
                printf("DATA 12\n");
                break;

	}


	printf("DATA: %d\n", D);
*/
        DoDraw();
}

void MyMainFrame::DMinus()
{
/*
        if(D<=0) D = 0; else
        D--;
	switch(D){
        case 0:
                Data = F9_776();
		fLD->SetText("9.776 GeV");
        	fLD->Layout();
                printf("DATA 0\n");
                break;
        case 1:
		Data = F13_761();
		fLD->SetText("13.761 GeV");
                fLD->Layout();
                printf("DATA 1\n");
                break;
	case 2:
		Data = F19_416();
		fLD->SetText("19.416 GeV");
                fLD->Layout();
                printf("DATA 2\n");
                break;
	case 3:
		Data = F23_46();
		fLD->SetText("23.46 GeV");
                fLD->Layout();
                printf("DATA 3\n");
                break;
	case 4:
		Data = F30_54();
		fLD->SetText("30.54 GeV");
                fLD->Layout();
                printf("DATA 4\n");
                break;
	case 5:
		Data = F44_64();
		fLD->SetText("44.64 GeV");
                fLD->Layout();
                printf("DATA 5\n");
                break;
	case 6:
		Data = F52_81();
		fLD->SetText("52.81 GeV");
                fLD->Layout();
                printf("DATA 6\n");
                break;
	case 7:
		Data = F62_07();
		fLD->SetText("62.07 GeV");
                fLD->Layout();
                printf("DATA 7\n");
                break;
	case 8: 
		Data = F546();
		fLD->SetText("546 GeV");
                fLD->Layout();
                printf("DATA 8\n");
                break;
	case 9:
		//Data = F630();
		Data = F630();
		fLD->SetText("630 GeV");
                fLD->Layout();
                printf("DATA 9\n");
                break;
	case 10:
		Data = F1800();
		fLD->SetText("1800 GeV");
                fLD->Layout();
                printf("DATA 10\n");
                break;
	case 11:
		Data = F1960();
		fLD->SetText("1960 GeV");
                fLD->Layout();
                printf("DATA 11\n");
                break;
	case 12:
		Data = F7000();
		//Data = F1960();
		fLD->SetText("7000 GeV");
                fLD->Layout();
                printf("DATA 12\n");
                break;
        }

	printf("DATA: %d\n", D);
*/
        DoDraw();
}





// funkcja zwraca ciag bedacy nazwa canvasu na podstawie podanych parametrow x y z t a g
Char_t * MyMainFrame::GetCanvasName(Double_t tt, Double_t aa, Double_t gg)
{
	
	Char_t *FF = new Char_t[200]; for(Int_t i=0; i<200; i++) FF[i] = '\0';
	sprintf(FF, "ROOT/WIDC2%.02fAVCH0%.02fTRAP%.02f.root", gg,aa,tt);


        //if(gg<0) sprintf(FF, "Theta_%02d__Phi_%03d__GTU_%03d", tt, aa, gg);
        //else sprintf(FF, "Theta_%02d__Phi_%03d__GTU_+%02d", tt, aa, gg);
        return FF;
}



/*
TGraph * MyMainFrame::ChiSquare(TGraphErrors *dane, TGraph *suma, Double_t  &ChOut)
{
        Double_t Chi2 = 0.0;
        Double_t x = 0.0;
        Double_t y = 0.0;

	TGraph *grchi = new TGraph();

        Int_t nPoints = dane->GetN();
	Double_t ErrorY;

        for(Int_t i=0; i<nPoints; i++)
        {
                dane->GetPoint(i, x, y);
		Double_t SumaY = suma->Eval(x);
		Double_t Diff = y - SumaY;
		ErrorY = dane->GetErrorY(i);
		Double_t Chi2ThisPoint = ((Diff*Diff)/(ErrorY*ErrorY));
		grchi->SetPoint(i, x, Chi2ThisPoint);
		Chi2 = Chi2 + Chi2ThisPoint;	
        }
	grchi->SetMarkerStyle(28);
	grchi->SetMarkerColor(kRed);
	grchi->GetXaxis()->SetTitle("Position of point on |t| axis");
	grchi->GetYaxis()->SetTitle("#Chi^{2} ");

	ChiSquareMin = Chi2;
	ChOut = Chi2;
        printf("CHI2: %f\n", Chi2);
	return grchi;
}
*/


/*
Double_t MyMainFrame::ChiSquare(TGraphErrors *dane, TGraph *suma)
{
        Double_t Chi2 = 0.0; Double_t x = 0.0; Double_t y = 0.0;
        TGraph *grchi = new TGraph();
        Int_t nPoints = dane->GetN();
        Double_t ErrorY;

        for(Int_t i=0; i<nPoints; i++)
        {
                dane->GetPoint(i, x, y);
                Double_t SumaY = suma->Eval(x);
                Double_t Diff = y - SumaY;
                ErrorY = dane->GetErrorY(i);
                Double_t Chi2ThisPoint = ((Diff*Diff)/(ErrorY*ErrorY));
                grchi->SetPoint(i, x, Chi2ThisPoint);
                Chi2 = Chi2 + Chi2ThisPoint;
        }
        grchi->SetMarkerStyle(28);
        grchi->SetMarkerColor(kRed);
        grchi->GetXaxis()->SetTitle("Position of point on |t| axis");
        grchi->GetYaxis()->SetTitle("#Chi^{2} ");

        //Double_t DChi2 = Chi2;
        return Chi2;
}
*/


/*
TLatex *MyMainFrame::Chi2Lat(TGraph *gr)
{
	Int_t nPoints = 0;
	Double_t Chi2 = 0.0;
	Double_t x = 0.0;
	Double_t y = 0.0;

	nPoints = gr->GetN();
	for(Int_t i=0; i<nPoints; i++)
	{
		gr->GetPoint(i, x, y);
		Chi2 = Chi2 + y;

	}



	TLatex *lat = new TLatex(0.8*gr->GetXaxis()->GetXmax(), 0.8*gr->GetYaxis()->GetXmax(), Form("Chi2: %.02e", Chi2));
	//lat->Draw();
	lat->SetTextSize(0.09);
	lat->SetTextColor(kRed);
	

	return lat;
}
*/

void MyMainFrame::DoDraw()
{
	cout<<"FajnaFunkcja"<<endl;

/*	
	printf("M: %f, K: %f, R: %f, A: %f, L: %f\n", M, K, R, A, L);
	printf("%s\n", NameAmplRe(M, K, A, R,1.0, 0.0, L));
	printf("%s\n", NameAmplIm(M, K, A, R, 1.0, 0.0,L));
	printf("%s\n", NameAmplSum(M, K, A, R, 1.0, 0.0,L));
	//return;


	Data->GetXaxis()->SetTitle("|t| [GeV]");
	Data->GetYaxis()->SetTitle("#frac{d#sigma}{d|t|} #left[ #frac{mb}{GeV} #right]");

	Double_t MH3, KB3, SX, SY;
	SX = 1.0; SY = 1.0; KB3 = 0.0; MH3 = 1.0;

	Double_t iM = M;
	Double_t iR = R;
	Double_t iK = K;
	Double_t iA = A;
	Double_t iL = L;


	system((Form("rm logs/log%03d.txt",nr)));
	
	FILE *fIn = fopen(Form("inputs/input%03d.txt",nr), "r");
        Float_t iM, iK, iR, iA, iL;
        Char_t bufIn[100]; for(Int_t i=0; i<100; i++) bufIn[i] = '\0';
        fgets(bufIn, 100, fIn);
        sscanf(bufIn, "%f %f %f %f %f", &iM, &iK, &iR, &iA, &iL);
        fclose(fIn);


	TSplotX *Splot;
	TFile *fOmega;
	TFile *fAmplituda;
	TPrzekroje *Prz;
	TGraph *grRe;
	TGraph *grIm;
	TGraph *grSum;
	TGraph *grOmega;
	TAmplituda *Amp;	


	//TThread::Lock();
	fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "readonly");
        grRe = (TGraph*)fAmplituda->Get(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
        grIm = (TGraph*)fAmplituda->Get(NameAmplIm(iM, iK, iA, iR,MH3, KB3,  iL));
        grSum = (TGraph*)fAmplituda->Get(NameAmplSum(iM, iK, iA, iR, MH3, KB3, iL));
        fAmplituda->Close();
	//TThread::UnLock();

        Data->SetTitle(Form("%s", NameAmplSum(iM, iK, iA, iR, MH3, KB3, iL)));

        if(grRe && grIm && grSum)
        {
		//TThread::Lock();
		fOmega = new TFile(Form("DBOmega/DBOmega.root"), "readonly");
                grOmega = (TGraph*)fOmega->Get(NameOmega(iM, iK, iA, iR, MH3, KB3));
                fOmega->Close();
		//TThread::UnLock();


		Prz = new TPrzekroje(grOmega);
	        //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
		Double_t DElast = Prz->PodajElastic(iL);
		Double_t DInelast = Prz->PodajInelastic(iL);
	        delete Prz; Prz = NULL;

	        //cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
		Double_t DChi2; 
		TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);
	        //cout<<Form("PROC%03d finished",nr)<<endl;
		TCanvas *cc = fEcan->GetCanvas();
                cc->cd();
		cc->SetLogy();	
		Data->Draw("AP");
		grSum->Draw("L same");		

		TLatex lt;
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


		cc->Update();

		TCanvas *cChi =  fEcanChi2->GetCanvas();
                cChi->cd();
                grChi2->Draw("AP");
                cChi->Update();

		printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));

	        return;
        } else

	{
		//TThread::Lock();		
		//fOmega = new TFile(Form("DBOmega/DBOmega%02d.root",nr), "readonly");
		fOmega = new TFile(Form("DBOmega/DBOmega.root"), "readonly");
                grOmega = (TGraph*)fOmega->Get(NameOmega(iM, iK, iA, iR, MH3, KB3));
                fOmega->Close();
		//TThread::UnLock();

		if(grOmega)
                {
                        Amp = new TAmplituda(grOmega);
                        grRe = Amp->PodajAmplitudeRe(iL);
                        grIm = Amp->PodajAmplitudeIm(iL);

                        grSum = SumaAmpl(grRe, grIm);
                        grSum->SetLineColor(kRed);
                        grRe->SetLineColor(kBlue);
                        grIm->SetLineColor(kGreen);

                        grRe->SetName(NameAmplRe(iM, iK, iA, iR,MH3, KB3, iL));
                        grRe->SetTitle(NameAmplRe(iM, iK, iA, iR,MH3, KB3, iL));
                        grIm->SetName(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
                        grIm->SetTitle(NameAmplIm(iM, iK, iA, iR,MH3, KB3,iL));
                        grSum->SetName(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
                        grSum->SetTitle(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));

			//TThread::Lock();
                        //fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda%02d.root",nr), "update");
			fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "update");
                        grRe->Write();
                        grIm->Write();
                        grSum->Write();
                        fAmplituda->Close();
			//TThread::UnLock();


			//Prz = new TPrzekroje(grOmega);
	                //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
        	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	//delete Prz; Prz = NULL;

	                //cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
        	        //cout<<Form("PROC%02d stopped",nr)<<endl;

			Prz = new TPrzekroje(grOmega);
	                //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
        	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	Double_t DElast = Prz->PodajElastic(iL);
                	Double_t DInelast = Prz->PodajInelastic(iL);
                	delete Prz; Prz = NULL;

                	//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
                	//Double_t DChi2 = ChiSquare(Data, grSum);
			Double_t DChi2; 
                	TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);
                	//cout<<Form("PROC%03d finished",nr)<<endl;


                	FILE *fd = fopen(Form("logs/log%03d.txt",nr), "a+");
                	fprintf(fd, "M= %.03f K= %.03f R= %.03f A= %.03f KB3: %f, MH3: %f\n", iM, iK, iR, iA, KB3, MH3);
                	fprintf(fd, "Chi: %f El: %f Inel: %f Total: %f Ratio: %f REIM: %f\n", DChi2, DElast, DInelast, DElast+DInelast, DElast/(DElast+DInelast), (TMath::Sqrt(grRe->Eval(0.001))/TMath::Sqrt(grIm->Eval(0.001))));
                	fprintf(fd, "END\n");
                	fclose(fd);

			TCanvas *cc = fEcan->GetCanvas();
	                cc->cd();
        	        cc->SetLogy();
                	Data->Draw("AP");
                	grSum->Draw("L same");
			
			TLatex lt;
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
       			lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


			

			cc->Update();

			TCanvas *cChi =  fEcanChi2->GetCanvas();
                        cChi->cd();
                        grChi2->Draw("AP");
                        cChi->Update();

			//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
			printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));
                	return;
		} else

		{

			Splot = new TSplotX();
        		grOmega = Splot->PodajCalke(iK, iM, iA, iR, KB3, MH3, SX, SY);
	        	delete Splot; Splot = NULL;

	        	grOmega->SetName(NameOmega(iM, iK, iA, iR, MH3, KB3));
		        grOmega->SetTitle(NameOmega(iM, iK, iA, iR, MH3, KB3));

			//TThread::Lock();
	        	//fOmega = new TFile(Form("DBOmega/DBOmega%02d.root",nr), "update");
			fOmega = new TFile(Form("DBOmega/DBOmega.root"), "update");
		        grOmega->Write();
        		fOmega->Close();
			//TThread::UnLock();

		        Amp = new TAmplituda(grOmega);
        		grRe = Amp->PodajAmplitudeRe(iL);
	        	grIm = Amp->PodajAmplitudeIm(iL);
	        	delete Amp; Amp = NULL;
	
		        grSum = SumaAmpl(grRe, grIm);
	
			grSum->SetLineColor(kRed);
		        grRe->SetLineColor(kBlue);
		        grIm->SetLineColor(kGreen);

		        grRe->SetName(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
		        grRe->SetTitle(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
	        	grIm->SetName(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
		        grIm->SetTitle(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
		        grSum->SetName(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
		        grSum->SetTitle(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
	
			//TThread::Lock();
			//fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda%02d.root",nr), "update");
			fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "update");
	        	grRe->Write();
		        grIm->Write();
		        grSum->Write();
	        	fAmplituda->Close();
			//TThread::UnLock();


			//Prz = new TPrzekroje(grOmega);
		        //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
	        	//cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
		        //delete Prz; Prz = NULL;

			//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;	
			//cout<<Form("PROC%02d stopped",nr)<<endl;	

			Prz = new TPrzekroje(grOmega);
                	//cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
                	//cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	Double_t DElast = Prz->PodajElastic(iL);
                	Double_t DInelast = Prz->PodajInelastic(iL);
                	delete Prz; Prz = NULL;

                	//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
                	//Double_t DChi2 = ChiSquare(Data, grSum);
                	//cout<<Form("PROC%03d finished",nr)<<endl;
			Double_t DChi2; 
                	TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);


                	FILE *fd = fopen(Form("logs/log%03d.txt",nr), "a+");
                	fprintf(fd, "M= %.03f K= %.03f R= %.03f A= %.03f KB3: %f, MH3: %f\n", iM, iK, iR, iA, KB3, MH3);
                	fprintf(fd, "Chi: %f El: %f Inel: %f Total: %f Ratio: %f REIM: %f\n", DChi2, DElast, DInelast, DElast+DInelast, DElast/(DElast+DInelast), (TMath::Sqrt(grRe->Eval(0.001))/TMath::Sqrt(grIm->Eval(0.001))));
                	fprintf(fd, "END\n");
                	fclose(fd);

			TCanvas *cc = fEcan->GetCanvas();
        	        cc->cd();
	                cc->SetLogy();
                	Data->Draw("AP");
                	grSum->Draw("L same");

			TLatex lt;
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


			cc->Update();
		
			TCanvas *cChi =  fEcanChi2->GetCanvas();
			cChi->cd();
			grChi2->Draw("AP");
			//grOmega->SetMarkerStyle(28);
			//grOmega->Draw("AP");
			cChi->Update();

			//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
			printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));
	
			return;
		}
	}


	//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
*/


}

/*
void MyMainFrame::SzukajMinimum(void)
{
	//DoDraw();
	Double_t MM = M;
	Double_t RR = R;
	Double_t KK = K;
	Double_t AA = A;
	Double_t LL = L;

	Double_t ChiThis;
	Double_t ChiPrevious;
	Double_t ChiNext;

	
		
	DoDraw();
	ChiThis = ChiSquareMin;
	M = MM+0.1;
	DoDraw();
	ChiNext = ChiSquareMin;
	M = MM-0.1;
	DoDraw();
	ChiPrevious = ChiSquareMin;
	

	while(!(ChiThis<ChiNext&&ChiThis<ChiPrevious))
	{

	printf("M: %.03f, %.02e, %.02e, %.02e\n",M, ChiPrevious, ChiThis, ChiNext);

	if(ChiThis>ChiNext)
	{
		ChiPrevious = ChiThis;
		ChiThis = ChiNext;
		MM = MM + 0.1;
		M = MM + 0.1;
		DoDraw();
		ChiNext = ChiSquareMin;
	} else 
	if(ChiThis>ChiPrevious)
	{
		ChiNext = ChiThis;
                ChiThis = ChiPrevious;
                MM = MM - 0.1;
                M = MM - 0.1;
                DoDraw();
                ChiPrevious = ChiSquareMin;
	}
	}

	M = MM;
	DoDraw();
		




	
        ChiThis = ChiSquareMin;
        R = RR+0.1;
        DoDraw();
        ChiNext = ChiSquareMin;
        R = RR-0.1;
	DoDraw();
        ChiPrevious = ChiSquareMin;

	 while(!(ChiThis<ChiNext&&ChiThis<ChiPrevious))
        {
	printf("R: %.03f, %.02e, %.02e, %.02e\n",R, ChiPrevious, ChiThis, ChiNext);

        if(ChiThis>ChiNext)
        {
                ChiPrevious = ChiThis;
                ChiThis = ChiNext;
                RR = RR + 0.1;
                R = RR + 0.1;
                DoDraw();
                ChiNext = ChiSquareMin;
        } else
        if(ChiThis>ChiPrevious)
        {
                ChiNext = ChiThis;
                ChiThis = ChiPrevious;
                RR = RR - 0.1;
		//if(R>0.05)
                R = RR - 0.1;
		//else R = 0.05;
		//if(R<0.0) R = 0.01;
		//printf("R: %.02f\n", R); 
                DoDraw();
                ChiPrevious = ChiSquareMin;
        }
        }

	R = RR;
        DoDraw();
	


	
	ChiThis = ChiSquareMin;
        K = KK+0.1;
        DoDraw();
        ChiNext = ChiSquareMin;
        K = KK-0.1;
	DoDraw();
        ChiPrevious = ChiSquareMin;

	 while(!(ChiThis<ChiNext&&ChiThis<ChiPrevious))
        {
	printf("K: %.03f, %.02e, %.02e, %.02e\n",K, ChiPrevious, ChiThis, ChiNext);

        if(ChiThis>ChiNext)
        {
                ChiPrevious = ChiThis;
                ChiThis = ChiNext;
                KK = KK + 0.1;
                K = KK + 0.1;
                DoDraw();
                ChiNext = ChiSquareMin;
        } else
        if(ChiThis>ChiPrevious)
        {
                ChiNext = ChiThis;
                ChiThis = ChiPrevious;
                KK = KK - 0.1;
                K = KK - 0.1;
                DoDraw();
                ChiPrevious = ChiSquareMin;
        }
        }
		

	K = KK;
        DoDraw();
	

	ChiThis = ChiSquareMin;
        A = AA+0.1;
        DoDraw();
        ChiNext = ChiSquareMin;
        A = AA-0.1;
	DoDraw();
        ChiPrevious = ChiSquareMin;
	 while(!(ChiThis<ChiNext&&ChiThis<ChiPrevious))
        {

	printf("A: %.03f, %.02e, %.02e, %.02e\n",A, ChiPrevious, ChiThis, ChiNext);

        if(ChiThis>ChiNext)
        {
                ChiPrevious = ChiThis;
                ChiThis = ChiNext;
                AA = AA + 0.1;
                A = AA + 0.1;
                DoDraw();
                ChiNext = ChiSquareMin;
        } else
        if(ChiThis>ChiPrevious)
        {
                ChiNext = ChiThis;
                ChiThis = ChiPrevious;
                AA = AA - 0.1;
                A = AA - 0.1;
                DoDraw();
                ChiPrevious = ChiSquareMin;
        }
        }

	A = AA;
        DoDraw();




	ChiThis = ChiSquareMin;
        L = LL+0.001;
        DoDraw();
        ChiNext = ChiSquareMin;
        L = LL-0.001;
	DoDraw();
        ChiPrevious = ChiSquareMin;

         while(!(ChiThis<ChiNext&&ChiThis<ChiPrevious))
        {

	printf("L: %.03f, %.02e, %.02e, %.02e\n",L, ChiPrevious, ChiThis, ChiNext);

        if(ChiThis>ChiNext)
        {
                ChiPrevious = ChiThis;
                ChiThis = ChiNext;
                LL = LL + 0.001;
                L = LL + 0.001;
                DoDraw();
                ChiNext = ChiSquareMin;
        } else
        if(ChiThis>ChiPrevious)
        {
                ChiNext = ChiThis;
                ChiThis = ChiPrevious;
                LL = LL - 0.001;
                L = LL - 0.001;
                DoDraw();
                ChiPrevious = ChiSquareMin;
        }
        }

	L = LL;
        DoDraw();

	//printf("ChiMin: %f\n", ChiMin);

	fLM->SetText(Form("%.03f", M));
        fLM->Layout();
        DoDraw();
	fLK->SetText(Form("%.02f", K));
        fLK->Layout();
        DoDraw();
	fLR->SetText(Form("%.02f", R));
        fLR->Layout();
        DoDraw();
	fLA->SetText(Form("%.02f", A));
        fLA->Layout();
        DoDraw();
	fLL->SetText(Form("%.03f", L));
        fLL->Layout();
        DoDraw();



}

*/
