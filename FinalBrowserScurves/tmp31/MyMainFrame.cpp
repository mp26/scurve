#include <TGFrame.h>
#include <TGLabel.h>
#include <TGListBox.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TFile.h>
#include <TH1F.h>
#include <TText.h>
#include <TList.h>
#include <TLatex.h>
#include <TH2F.h>

#include <Riostream.h>
#include "TObject.h"
#include "TLine.h"

#include <TColor.h>
#include <TROOT.h>
#include <TGClient.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphErrors.h>
//#include "Grafy.h"
//#include "SRC/TAmplituda.h"
//#include "SRC/TSplotX.h"
//#include "SRC/TPrzekroje.h"

#include "Exec.h"

Int_t X;
Int_t Y;
//Int_t DAC;
//Int_t PMT;

class MyMainFrame : public TGMainFrame {
	
	private:
	TGMainFrame *fMainFrame;		// Glowna ramka (zawsze)
	TGHorizontalFrame *fHFrameCanvas;		// Pionowa ramka dla zelaza
	TGHorizontalFrame *fHFrameButtons;	// Pionowa ramka dla protonu
	
	TGGroupFrame *fGFrameCanvas, *fGFrameButtons, *fGFrameData;	// Pionowe ramki (z obramowaniem i podpisem, grupowe)

	TRootEmbeddedCanvas  *fEcan;	// Canvas zalaczony w oknie
	TCanvas *fCanvas;
	TPad *fPad;
//	TPad *gPad;
	TCanvas *gCanvas;

	TGGroupFrame *fGD,  *fGDac, *fGPMT, *fGSave;

	TGTextButton *fDPlus, *fDMinus, *fDacPlus, *fDacMinus, *fPmtPlus, *fPmtMinus, *fDacSave;

	TGTextButton *fSzukaj;

	TGLabel *fLD, *fLDac, *fLPmt, *fLSave;

	Double_t EstimatedDacPMT[36];	// Tablica wyznaczonych wartości DAC10
	Bool_t SavedOrNot[36];

	TLine *lDac;
	TLine *lDacMinus50;
	
	public:
	MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
	virtual ~MyMainFrame();
	void DoExit();
	Char_t * GetCanvasName(Double_t tt, Double_t aa, Double_t gg);




	void DPlus();
	void DMinus();

	void DacPlus();
        void DacMinus();

	void PmtPlus();
        void PmtMinus();

	void Save();

	void DoDraw();

	void XYFromPixelNumber(Int_t n);
//	void myexec();
	void ReadEstimatedDacs(TString DacFileName);
	Int_t GetnPMTFromXY(Int_t x, Int_t y);
	//void XYFromPixelNumberInPMT(Int_t n);
	void GetXYfromPMT(Int_t nPMT, Int_t &xx, Int_t &yy);
	void x16();
//	void SaveGraphs();
	// ----------------------------------------

	TFile *fIn;

	//Int_t X;// = 40;
	//Int_t Y;// = 40;


	// Tutaj ustawić startowy pixel !!!
	Int_t nPix;// = 0;
	Int_t DAC;
	Int_t PMT;

	TCanvas *tmpCanvas;

	TString canv_name; //= Form("c_Scurve_y%02d_x%02d", y, x);

	ClassDef(MyMainFrame,0);
};


MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h) : TGMainFrame(p, w, h)
{
	//ReadEstimatedDacs("BajkonurDAC10_MP_1100V.txt");
	// or
	//ReadEstimatedDacs("BajkonurDAC10_ZP_1100V.txt");

	//ReadEstimatedDacs("EstimatedDac10.txt");
	//ReadEstimatedDacs("lechDACs.txt");
	ReadEstimatedDacs("Lech_MarikaDACs.txt");
		

	//fIn = new TFile("Scurves/Bajkonur/lechscreenscurves/ScurveOut_SC__2019_08_12__15_03_08__3782,3782,3782,3782,3782,3782,3782,3782,3782__dvr1050_lechscreen_12cloth_1nW_cathode3.root", "readonly");
	//fIn = new TFile("ScurveBajkonur/ScurveOut_SC__2019_07_01__20_03_00__3422,3422,3422,3422,3422,3422,3422,3422,3422__dvr950_poster_disc.root", "readonly");



	//fIn = new TFile("/home/ncbj/Pulpit/GitLab/scurve/FinalBrowserScurves/tmp31/Scurves/Bajkonur/2019-08-13-baikonur/ScurveOut_SC__2019_08_13__12_24_16__3962,3962,3962,3962,3962,3962,3962,3962,3962__dvr1100_lechscreen_4cloth_cathode3.root", "readonly");
	//fIn = new TFile("/home/ncbj/Pulpit/GitLab/scurve/FinalBrowserScurves/tmp31/Scurves/Bajkonur/2019-08-13-baikonur/ScurveOut_SC__2019_08_13__12_18_00__3422,3422,3422,3422,3422,3422,3422,3422,3422__dvr950_lechscreen_4cloth_cathode3.root", "readonly");





	fIn = new TFile("Scurves/Bajkonur/2019-08-13-baikonur/ScurveOut_SC__2019_08_13__12_34_09__3962,3962,3962,3962,3962,3962,3962,3962,3962__dvr1100_lechscreen_4cloth_cathode2.root", "readonly");



	//for(Int_t i=0; i<36; i++) EstimatedDacPMT[i] = 0;
	for(Int_t i=0; i<36; i++) SavedOrNot[i] = kFALSE;
	lDac = new TLine();
	lDacMinus50 = new TLine();

	
	lDac->SetY1(1.e6);
        lDac->SetY2(1.e-6);
	lDac->SetLineStyle(9);
	lDac->SetLineWidth(2.0);
	lDac->SetLineColor(kRed);


	lDacMinus50->SetY1(1.e6);
        lDacMinus50->SetY2(1.e-6);
        lDacMinus50->SetLineStyle(9);
        lDacMinus50->SetLineWidth(2.0);
        lDacMinus50->SetLineColor(kGreen);


	PMT = 1;
	//DAC = 380;
	nPix = 0;


	// Main Frame
	fMainFrame = new TGMainFrame(0, 10, 10, kMainFrame | kVerticalFrame);
	fMainFrame->SetLayoutBroken(kTRUE);


	//__________________________________________________________________________	
	// CANVAS
	fHFrameCanvas = new TGHorizontalFrame(fMainFrame, 375, 380, kHorizontalFrame);
	fGFrameCanvas = new TGGroupFrame(fHFrameCanvas, "CANVAS");
	fGFrameCanvas->SetLayoutBroken(kTRUE);

	//____________________________________________
	// TUTAJ CIALO PROGRAMU DLA CANVAS
	fEcan = new TRootEmbeddedCanvas(0,fGFrameCanvas,500,300);
   	fGFrameCanvas->AddFrame(fEcan, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                     kLHintsExpandX  | kLHintsExpandY,0,0,1,1));
	fEcan->MoveResize(10,20,700,600);
	//fEcan->SetLogy();

	fCanvas = (TCanvas*)fEcan->GetCanvas();
        fCanvas->AddExec("myExec", "myexec(X,Y)");
	//fCanvas->AddExec("myExec","MyMainFrame::myexec()");

	//_______________________________________________________________________

	fHFrameCanvas->AddFrame(fGFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fGFrameCanvas->MoveResize(5,5, 720, 640);
	fMainFrame->AddFrame(fHFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fHFrameCanvas->MoveResize(10,10,730 ,650);
	// END OF CANVAS
	//_____________________________________________________________________________________



	//___________________________________________________________________________
	//BUTTONS
	fHFrameButtons = new TGHorizontalFrame(fMainFrame, 175, 380, kHorizontalFrame);
        fGFrameButtons = new TGGroupFrame(fHFrameButtons, "PARAMETERS");
        fGFrameButtons->SetLayoutBroken(kTRUE);

	//________________________________________________ 
	// TUTAJ CIALO PROGRAMU DLA BUTTONS

	// **************** Save ***************************
        fGSave = new TGGroupFrame(fGFrameButtons, "PMT:");
        fGSave->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- Save ---------------------
        fDacSave = new TGTextButton(fGSave, "&SAVE");
        fDacSave->Connect("Pressed()", "MyMainFrame", this, "Save()");
        fGSave->AddFrame(fDacSave, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDacSave->MoveResize(10, 20, 130,40);

        //fPmtMinus = new TGTextButton(fGPMT, "&MINUS");
        //fPmtMinus->Connect("Pressed()", "MyMainFrame", this, "PmtMinus()");
        //fGPMT->AddFrame(fPmtMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        //fPmtMinus->MoveResize(80, 15, 60,30);
        // ----------------------------------------------------------------

	// pole do pisania wartości
        fLSave = new TGLabel(fGSave, "-----");
        fGSave->AddFrame(fLSave, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLSave->MoveResize(10, 65, 130, 20);


        fGFrameButtons->AddFrame(fGSave, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGSave->MoveResize(5,340,155, 110);
        // *********************




	// **************** PMT ***************************
        fGPMT = new TGGroupFrame(fGFrameButtons, "PMT:");
        fGPMT->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- PMT ---------------------
        fPmtPlus = new TGTextButton(fGPMT, "&PLUS");
        fPmtPlus->Connect("Pressed()", "MyMainFrame", this, "PmtPlus()");
        fGPMT->AddFrame(fPmtPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fPmtPlus->MoveResize(10, 15, 60,30);

        fPmtMinus = new TGTextButton(fGPMT, "&MINUS");
        fPmtMinus->Connect("Pressed()", "MyMainFrame", this, "PmtMinus()");
        fGPMT->AddFrame(fPmtMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fPmtMinus->MoveResize(80, 15, 60,30);
        // ----------------------------------------------------------------

        fLPmt = new TGLabel(fGPMT, "0.73 ");
        fGPMT->AddFrame(fLPmt, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLPmt->MoveResize(10, 55, 140, 20);


        fGFrameButtons->AddFrame(fGPMT, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGPMT->MoveResize(5,240,155, 90);
        // *********************



	// **************** DAC ***************************
        fGDac = new TGGroupFrame(fGFrameButtons, "DAC:");
        fGDac->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- DAC ---------------------
        fDacPlus = new TGTextButton(fGDac, "&PLUS");
        fDacPlus->Connect("Pressed()", "MyMainFrame", this, "DacPlus()");
        fGDac->AddFrame(fDacPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDacPlus->MoveResize(10, 15, 60,30);

        fDacMinus = new TGTextButton(fGDac, "&MINUS");
        fDacMinus->Connect("Pressed()", "MyMainFrame", this, "DacMinus()");
        fGDac->AddFrame(fDacMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDacMinus->MoveResize(80, 15, 60,30);
        // ----------------------------------------------------------------

        fLDac = new TGLabel(fGDac, "0.73 ");
        fGDac->AddFrame(fLDac, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLDac->MoveResize(10, 55, 140, 20);


        fGFrameButtons->AddFrame(fGDac, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGDac->MoveResize(5,140,155, 90);
        // *********************




	// **************** D ***************************
        fGD = new TGGroupFrame(fGFrameButtons, "DATA:");
        fGD->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- D 
        fDPlus = new TGTextButton(fGD, "&PLUS");
        fDPlus->Connect("Pressed()", "MyMainFrame", this, "DPlus()");
        fGD->AddFrame(fDPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDPlus->MoveResize(10, 15, 60,30);

        fDMinus = new TGTextButton(fGD, "&MINUS");
        fDMinus->Connect("Pressed()", "MyMainFrame", this, "DMinus()");
        fGD->AddFrame(fDMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDMinus->MoveResize(80, 15, 60,30);
        // -----------------------------------------------------------------

	fGD->SetBackgroundColor(gROOT->GetColor(kGray)->GetPixel());
	fLD = new TGLabel(fGD, "1960 GeV");
        fGD->AddFrame(fLD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLD->MoveResize(10, 55, 140, 20);


	fSzukaj = new TGTextButton(fGD, "&x16");
	fSzukaj->Connect("Pressed()", "MyMainFrame", this, "x16()");
	fGD->AddFrame(fSzukaj, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fSzukaj->MoveResize(10, 85, 140, 20);


        fGFrameButtons->AddFrame(fGD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGD->MoveResize(5,15,155, 120);
        // *********************


	//________________________________________________________________________________________

        fHFrameButtons->AddFrame(fGFrameButtons, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGFrameButtons->MoveResize(5,15, 165, 600);



        fMainFrame->AddFrame(fHFrameButtons, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fHFrameButtons->MoveResize(740,10,200 ,640);	
	//END OF BUTTONS
	//___________________________________________________________________________________________


	// Configuration of main frame
	fMainFrame->SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
	fMainFrame->MapSubwindows();
	fMainFrame->SetWindowName("Moje Nowe Okno");
	fMainFrame->Resize(fMainFrame->GetDefaultSize());
	fMainFrame->MapWindow();
	fMainFrame->Resize(w, h);
	fMainFrame->Layout();

	//DPlus();
	PmtMinus();

}	

MyMainFrame::~MyMainFrame()
{
	Cleanup();
}


void MyMainFrame::DoExit()
{
	gApplication->Terminate(0);
}


void MyMainFrame::Save()
{
	EstimatedDacPMT[PMT-1] = 1008 - DAC;	


	FILE *fOut = fopen("TestDac.txt", "w");
        for(Int_t i=0; i<6; i++)
                fprintf(fOut, "%d %d %d %d %d %d\n", (Int_t)EstimatedDacPMT[i*6], (Int_t)EstimatedDacPMT[i*6+1], (Int_t)EstimatedDacPMT[i*6+2], (Int_t)EstimatedDacPMT[i*6+3], (Int_t)EstimatedDacPMT[i*6+4], (Int_t)EstimatedDacPMT[i*6+5]);

        fclose(fOut);

	SavedOrNot[PMT-1] = kTRUE;

	if(SavedOrNot[PMT-1]) 
	{
		fLSave->SetBackgroundColor(gROOT->GetColor(kGreen)->GetPixel());
		fLSave->SetText("SAVED"); 
	}
	else 
	{
		fLSave->SetBackgroundColor(gROOT->GetColor(kRed)->GetPixel());
		fLSave->SetText("-----");
	}

        fLSave->Layout();
}


void MyMainFrame::DPlus()
{
	do
	{
	nPix = nPix +1;
	if(nPix>2303) nPix=0;
        DoDraw();
	}while(GetnPMTFromXY(Y,X)!=PMT);
}

void MyMainFrame::DMinus()
{
	do
	{	
		nPix = nPix - 1;
		if(nPix<0) nPix=2303;
        DoDraw();
	}while(GetnPMTFromXY(Y,X)!=PMT);
}


void MyMainFrame::DacPlus()
{
	DAC = DAC + 1;
        if(DAC>1023) DAC=1023;
        DoDraw();
}

void MyMainFrame::DacMinus()
{
	DAC = DAC - 1;
        if(DAC<0) DAC=0;
        DoDraw();
}

void MyMainFrame::PmtPlus()
{
        PMT = PMT + 1;
        if(PMT>36) PMT=36;
	DAC = 1008 - EstimatedDacPMT[PMT-1];
	DPlus();
//	DAC = 1008 - EstimatedDacPMT[PMT-1];
        DoDraw();
	//if(SavedOrNot[PMT]) fLSave->SetText("SAVED"); else fLSave->SetText("-----");
        if(SavedOrNot[PMT-1]) 
        {
		fLSave->SetBackgroundColor(gROOT->GetColor(kGreen)->GetPixel());
                fLSave->SetText("SAVED"); 
        }
        else
        {
                fLSave->SetBackgroundColor(gROOT->GetColor(kRed)->GetPixel());
                fLSave->SetText("-----");
        }
	fLSave->Layout();
}

void MyMainFrame::PmtMinus()
{
        PMT = PMT - 1;
        if(PMT<1) PMT=1;
	DAC = 1008 - EstimatedDacPMT[PMT-1];
	DPlus();
	//DAC = 1008 - EstimatedDacPMT[PMT];
        DoDraw();
	//if(SavedOrNot[PMT]) fLSave->SetText("SAVED"); else fLSave->SetText("-----");
        if(SavedOrNot[PMT-1]) 
        {
		fLSave->SetBackgroundColor(gROOT->GetColor(kGreen)->GetPixel());
                fLSave->SetText("SAVED"); 
        }
        else
        {
                fLSave->SetBackgroundColor(gROOT->GetColor(kRed)->GetPixel());
                fLSave->SetText("-----");
        }
	fLSave->Layout();
}

void MyMainFrame::x16()
{
	for(Int_t i=0; i<16; i++)
	{
		DPlus();
		//gSystem->Sleep(250);	
	}

}



// funkcja zwraca ciag bedacy nazwa canvasu na podstawie podanych parametrow x y z t a g
Char_t * MyMainFrame::GetCanvasName(Double_t tt, Double_t aa, Double_t gg)
{
	
	Char_t *FF = new Char_t[200]; for(Int_t i=0; i<200; i++) FF[i] = '\0';
	sprintf(FF, "ROOT/WIDC2%.02fAVCH0%.02fTRAP%.02f.root", gg,aa,tt);


        //if(gg<0) sprintf(FF, "Theta_%02d__Phi_%03d__GTU_%03d", tt, aa, gg);
        //else sprintf(FF, "Theta_%02d__Phi_%03d__GTU_+%02d", tt, aa, gg);
        return FF;
}



/*
TGraph * MyMainFrame::ChiSquare(TGraphErrors *dane, TGraph *suma, Double_t  &ChOut)
{
        Double_t Chi2 = 0.0;
        Double_t x = 0.0;
        Double_t y = 0.0;

	TGraph *grchi = new TGraph();

        Int_t nPoints = dane->GetN();
	Double_t ErrorY;

        for(Int_t i=0; i<nPoints; i++)
        {
                dane->GetPoint(i, x, y);
		Double_t SumaY = suma->Eval(x);
		Double_t Diff = y - SumaY;
		ErrorY = dane->GetErrorY(i);
		Double_t Chi2ThisPoint = ((Diff*Diff)/(ErrorY*ErrorY));
		grchi->SetPoint(i, x, Chi2ThisPoint);
		Chi2 = Chi2 + Chi2ThisPoint;	
        }
	grchi->SetMarkerStyle(28);
	grchi->SetMarkerColor(kRed);
	grchi->GetXaxis()->SetTitle("Position of point on |t| axis");
	grchi->GetYaxis()->SetTitle("#Chi^{2} ");

	ChiSquareMin = Chi2;
	ChOut = Chi2;
        printf("CHI2: %f\n", Chi2);
	return grchi;
}
*/


/*
Double_t MyMainFrame::ChiSquare(TGraphErrors *dane, TGraph *suma)
{
        Double_t Chi2 = 0.0; Double_t x = 0.0; Double_t y = 0.0;
        TGraph *grchi = new TGraph();
        Int_t nPoints = dane->GetN();
        Double_t ErrorY;

        for(Int_t i=0; i<nPoints; i++)
        {
                dane->GetPoint(i, x, y);
                Double_t SumaY = suma->Eval(x);
                Double_t Diff = y - SumaY;
                ErrorY = dane->GetErrorY(i);
                Double_t Chi2ThisPoint = ((Diff*Diff)/(ErrorY*ErrorY));
                grchi->SetPoint(i, x, Chi2ThisPoint);
                Chi2 = Chi2 + Chi2ThisPoint;
        }
        grchi->SetMarkerStyle(28);
        grchi->SetMarkerColor(kRed);
        grchi->GetXaxis()->SetTitle("Position of point on |t| axis");
        grchi->GetYaxis()->SetTitle("#Chi^{2} ");

        //Double_t DChi2 = Chi2;
        return Chi2;
}
*/



void MyMainFrame::DoDraw()
{
	//cout<<"FajnaFunkcja"<<endl;

	TObject *obj, *clone;
	fLD->SetText(Form("PIXEL: %d", nPix));
        fLD->Layout();
	fLDac->SetText(Form("1008 - DAC: %d", DAC));
        fLDac->Layout();
	fLPmt->SetText(Form("PMT: %d", PMT));
        fLPmt->Layout();
	//if(SavedOrNot[PMT]) fLSave->SetText("SAVED"); else fLSave->SetText("-----");
        if(SavedOrNot[PMT-1]) 
        {
		fLSave->SetBackgroundColor(gROOT->GetColor(kGreen)->GetPixel());
                fLSave->SetText("SAVED"); 
        }
        else
        {
                fLSave->SetBackgroundColor(gROOT->GetColor(kRed)->GetPixel());
                fLSave->SetText("-----");
        }
	fLSave->Layout();


	XYFromPixelNumber(nPix);
	//XYFromPixelNumberInPMT(nPix);

	if(GetnPMTFromXY(Y,X)==PMT)
	{


	TCanvas *tmpCanvas = (TCanvas*)fIn->Get(Form("c_Scurve_y%02d_x%02d", Y, X))->Clone();
	fCanvas = (TCanvas*)fEcan->GetCanvas();
	fCanvas->SetLogy();	
	fPad = (TPad*)fCanvas->GetPad(0);

	fPad->GetListOfPrimitives()->RemoveAll();

	TIter next(tmpCanvas->GetListOfPrimitives());
  	while ((obj=next()))
	{
     		gROOT->SetSelectedPad(fPad);
     		clone = obj->Clone();
     		fPad->GetListOfPrimitives()->Add(clone,obj->GetDrawOption());
  	}

	TH1F *hh = (TH1F*)fCanvas->FindObject(  Form("h_Scurve_y%02d_x%02d", Y, X)     );
	hh->SetTitle(Form("h_Scurve: x=%02d y=%02d", Y, X));
	hh->GetXaxis()->SetRange(200, 700);
	hh->SetMaximum(3.e2);
	hh->SetMinimum(1.e-6);

	

	cout<<"Dac10: "<<EstimatedDacPMT[GetnPMTFromXY(Y,X)-1]<<endl;
	cout<<"X: "<<X<<" Y: "<<Y<<endl;
	cout<<"nPMT: "<<GetnPMTFromXY(Y,X)<<endl;


	// If DAC estimated from FILE
	//lDac->SetX1(1008 - EstimatedDacPMT[GetnPMTFromXY(Y,X)-1]);
	//lDac->SetX2(1008 - EstimatedDacPMT[GetnPMTFromXY(Y,X)-1]);
	
	lDac->SetY1(hh->GetMaximum());
	lDac->SetY2(hh->GetMinimum());

	lDacMinus50->SetY1(hh->GetMaximum());
        lDacMinus50->SetY2(hh->GetMinimum());


	// If DAC set by button
	lDac->SetX1(DAC);
	lDac->SetX2(DAC);

	lDacMinus50->SetX1(DAC-50);
        lDacMinus50->SetX2(DAC-50);


	//tmpCanvas = (TCanvas*)fIn->Get(Form("c_Scurve_y%02d_x%02d", Y, X))->Clone();
	//
	
	//if(GetnPMTFromXY(Y,X)==PMT)
	
		fCanvas->SetLogy();
		fCanvas->Draw();
		lDac->Draw("same");
		lDacMinus50->Draw("same");
		fCanvas->Update();
 fCanvas->Print(Form("EPS/X_%02d__Y_%02d__PMT__%02d.eps", Y,X,GetnPMTFromXY(Y,X)));

	}

/*	
	printf("M: %f, K: %f, R: %f, A: %f, L: %f\n", M, K, R, A, L);
	printf("%s\n", NameAmplRe(M, K, A, R,1.0, 0.0, L));
	printf("%s\n", NameAmplIm(M, K, A, R, 1.0, 0.0,L));
	printf("%s\n", NameAmplSum(M, K, A, R, 1.0, 0.0,L));
	//return;


	Data->GetXaxis()->SetTitle("|t| [GeV]");
	Data->GetYaxis()->SetTitle("#frac{d#sigma}{d|t|} #left[ #frac{mb}{GeV} #right]");

	Double_t MH3, KB3, SX, SY;
	SX = 1.0; SY = 1.0; KB3 = 0.0; MH3 = 1.0;

	Double_t iM = M;
	Double_t iR = R;
	Double_t iK = K;
	Double_t iA = A;
	Double_t iL = L;


	system((Form("rm logs/log%03d.txt",nr)));
	
	FILE *fIn = fopen(Form("inputs/input%03d.txt",nr), "r");
        Float_t iM, iK, iR, iA, iL;
        Char_t bufIn[100]; for(Int_t i=0; i<100; i++) bufIn[i] = '\0';
        fgets(bufIn, 100, fIn);
        sscanf(bufIn, "%f %f %f %f %f", &iM, &iK, &iR, &iA, &iL);
        fclose(fIn);


	TSplotX *Splot;
	TFile *fOmega;
	TFile *fAmplituda;
	TPrzekroje *Prz;
	TGraph *grRe;
	TGraph *grIm;
	TGraph *grSum;
	TGraph *grOmega;
	TAmplituda *Amp;	


	//TThread::Lock();
	fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "readonly");
        grRe = (TGraph*)fAmplituda->Get(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
        grIm = (TGraph*)fAmplituda->Get(NameAmplIm(iM, iK, iA, iR,MH3, KB3,  iL));
        grSum = (TGraph*)fAmplituda->Get(NameAmplSum(iM, iK, iA, iR, MH3, KB3, iL));
        fAmplituda->Close();
	//TThread::UnLock();

        Data->SetTitle(Form("%s", NameAmplSum(iM, iK, iA, iR, MH3, KB3, iL)));

        if(grRe && grIm && grSum)
        {
		//TThread::Lock();
		fOmega = new TFile(Form("DBOmega/DBOmega.root"), "readonly");
                grOmega = (TGraph*)fOmega->Get(NameOmega(iM, iK, iA, iR, MH3, KB3));
                fOmega->Close();
		//TThread::UnLock();


		Prz = new TPrzekroje(grOmega);
	        //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
		Double_t DElast = Prz->PodajElastic(iL);
		Double_t DInelast = Prz->PodajInelastic(iL);
	        delete Prz; Prz = NULL;

	        //cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
		Double_t DChi2; 
		TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);
	        //cout<<Form("PROC%03d finished",nr)<<endl;
		TCanvas *cc = fEcan->GetCanvas();
                cc->cd();
		cc->SetLogy();	
		Data->Draw("AP");
		grSum->Draw("L same");		

		TLatex lt;
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


		cc->Update();

		TCanvas *cChi =  fEcanChi2->GetCanvas();
                cChi->cd();
                grChi2->Draw("AP");
                cChi->Update();

		printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));

	        return;
        } else

	{
		//TThread::Lock();		
		//fOmega = new TFile(Form("DBOmega/DBOmega%02d.root",nr), "readonly");
		fOmega = new TFile(Form("DBOmega/DBOmega.root"), "readonly");
                grOmega = (TGraph*)fOmega->Get(NameOmega(iM, iK, iA, iR, MH3, KB3));
                fOmega->Close();
		//TThread::UnLock();

		if(grOmega)
                {
                        Amp = new TAmplituda(grOmega);
                        grRe = Amp->PodajAmplitudeRe(iL);
                        grIm = Amp->PodajAmplitudeIm(iL);

                        grSum = SumaAmpl(grRe, grIm);
                        grSum->SetLineColor(kRed);
                        grRe->SetLineColor(kBlue);
                        grIm->SetLineColor(kGreen);

                        grRe->SetName(NameAmplRe(iM, iK, iA, iR,MH3, KB3, iL));
                        grRe->SetTitle(NameAmplRe(iM, iK, iA, iR,MH3, KB3, iL));
                        grIm->SetName(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
                        grIm->SetTitle(NameAmplIm(iM, iK, iA, iR,MH3, KB3,iL));
                        grSum->SetName(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
                        grSum->SetTitle(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));

			//TThread::Lock();
                        //fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda%02d.root",nr), "update");
			fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "update");
                        grRe->Write();
                        grIm->Write();
                        grSum->Write();
                        fAmplituda->Close();
			//TThread::UnLock();


			//Prz = new TPrzekroje(grOmega);
	                //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
        	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	//delete Prz; Prz = NULL;

	                //cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
        	        //cout<<Form("PROC%02d stopped",nr)<<endl;

			Prz = new TPrzekroje(grOmega);
	                //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
        	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	Double_t DElast = Prz->PodajElastic(iL);
                	Double_t DInelast = Prz->PodajInelastic(iL);
                	delete Prz; Prz = NULL;

                	//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
                	//Double_t DChi2 = ChiSquare(Data, grSum);
			Double_t DChi2; 
                	TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);
                	//cout<<Form("PROC%03d finished",nr)<<endl;


                	FILE *fd = fopen(Form("logs/log%03d.txt",nr), "a+");
                	fprintf(fd, "M= %.03f K= %.03f R= %.03f A= %.03f KB3: %f, MH3: %f\n", iM, iK, iR, iA, KB3, MH3);
                	fprintf(fd, "Chi: %f El: %f Inel: %f Total: %f Ratio: %f REIM: %f\n", DChi2, DElast, DInelast, DElast+DInelast, DElast/(DElast+DInelast), (TMath::Sqrt(grRe->Eval(0.001))/TMath::Sqrt(grIm->Eval(0.001))));
                	fprintf(fd, "END\n");
                	fclose(fd);

			TCanvas *cc = fEcan->GetCanvas();
	                cc->cd();
        	        cc->SetLogy();
                	Data->Draw("AP");
                	grSum->Draw("L same");
			
			TLatex lt;
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
       			lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


			

			cc->Update();

			TCanvas *cChi =  fEcanChi2->GetCanvas();
                        cChi->cd();
                        grChi2->Draw("AP");
                        cChi->Update();

			//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
			printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));
                	return;
		} else

		{

			Splot = new TSplotX();
        		grOmega = Splot->PodajCalke(iK, iM, iA, iR, KB3, MH3, SX, SY);
	        	delete Splot; Splot = NULL;

	        	grOmega->SetName(NameOmega(iM, iK, iA, iR, MH3, KB3));
		        grOmega->SetTitle(NameOmega(iM, iK, iA, iR, MH3, KB3));

			//TThread::Lock();
	        	//fOmega = new TFile(Form("DBOmega/DBOmega%02d.root",nr), "update");
			fOmega = new TFile(Form("DBOmega/DBOmega.root"), "update");
		        grOmega->Write();
        		fOmega->Close();
			//TThread::UnLock();

		        Amp = new TAmplituda(grOmega);
        		grRe = Amp->PodajAmplitudeRe(iL);
	        	grIm = Amp->PodajAmplitudeIm(iL);
	        	delete Amp; Amp = NULL;
	
		        grSum = SumaAmpl(grRe, grIm);
	
			grSum->SetLineColor(kRed);
		        grRe->SetLineColor(kBlue);
		        grIm->SetLineColor(kGreen);

		        grRe->SetName(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
		        grRe->SetTitle(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
	        	grIm->SetName(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
		        grIm->SetTitle(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
		        grSum->SetName(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
		        grSum->SetTitle(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
	
			//TThread::Lock();
			//fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda%02d.root",nr), "update");
			fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "update");
	        	grRe->Write();
		        grIm->Write();
		        grSum->Write();
	        	fAmplituda->Close();
			//TThread::UnLock();


			//Prz = new TPrzekroje(grOmega);
		        //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
	        	//cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
		        //delete Prz; Prz = NULL;

			//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;	
			//cout<<Form("PROC%02d stopped",nr)<<endl;	

			Prz = new TPrzekroje(grOmega);
                	//cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
                	//cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	Double_t DElast = Prz->PodajElastic(iL);
                	Double_t DInelast = Prz->PodajInelastic(iL);
                	delete Prz; Prz = NULL;

                	//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
                	//Double_t DChi2 = ChiSquare(Data, grSum);
                	//cout<<Form("PROC%03d finished",nr)<<endl;
			Double_t DChi2; 
                	TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);


                	FILE *fd = fopen(Form("logs/log%03d.txt",nr), "a+");
                	fprintf(fd, "M= %.03f K= %.03f R= %.03f A= %.03f KB3: %f, MH3: %f\n", iM, iK, iR, iA, KB3, MH3);
                	fprintf(fd, "Chi: %f El: %f Inel: %f Total: %f Ratio: %f REIM: %f\n", DChi2, DElast, DInelast, DElast+DInelast, DElast/(DElast+DInelast), (TMath::Sqrt(grRe->Eval(0.001))/TMath::Sqrt(grIm->Eval(0.001))));
                	fprintf(fd, "END\n");
                	fclose(fd);

			TCanvas *cc = fEcan->GetCanvas();
        	        cc->cd();
	                cc->SetLogy();
                	Data->Draw("AP");
                	grSum->Draw("L same");

			TLatex lt;
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


			cc->Update();
		
			TCanvas *cChi =  fEcanChi2->GetCanvas();
			cChi->cd();
			grChi2->Draw("AP");
			//grOmega->SetMarkerStyle(28);
			//grOmega->Draw("AP");
			cChi->Update();

			//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
			printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));
	
			return;
		}
	}


	//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
*/


}


//void MyMainFrame::XYFromPixelNumberInPMT(Int_t n)
//{
//	Int_t xStart = 0; Int_t yStart = 0;
//	GetXYfromPMT(PMT, xStart, yStart);
//	cout<<"PMT: "<<PMT<<endl;
//	cout<<"xStart: "<<xStart<<endl;
//	cout<<"yStart: "<<yStart<<endl;


//        for(Int_t y=xStart; y<xStart+8; y++)
//                for(Int_t x=yStart; x<yStart+8; x++)
//                        if( (x*8+y)==n)
//                        {
//                                X = x;
//                                Y = y;
//                                cout<<"n: "<<n<<endl;
//                                cout<<"X: "<<X<<endl;
//                                cout<<"Y: "<<Y<<endl;
//                                return;
//                        }
//        cout<<"Cannot determine X Y coordinates !!!!"<<endl;
//        return;
//}


void MyMainFrame::XYFromPixelNumber(Int_t n)
{
	for(Int_t x=0; x<48; x++)
		for(Int_t y=0; y<48; y++)
			if( (x*48+y)==n)
			{
				X = x;
				Y = y;
				//cout<<"n: "<<n<<endl;
				//cout<<"X: "<<X<<endl;
				//cout<<"Y: "<<Y<<endl;
				return;
			}
	cout<<"Cannot determine X Y coordinates !!!!"<<endl;
	return;
}

/*
void MyMainFrame::myexec()
{
   // get event information
   int event = gPad->GetEvent();
   //myEvent = event;
   int px    = gPad->GetEventX();
   int py    = gPad->GetEventY();

   //if(event==1) gApplication->Terminate(0);
   // some magic to get the coordinates...
   double xd = gPad->AbsPixeltoX(px);
   double yd = gPad->AbsPixeltoY(py);
   float x = gPad->PadtoX(xd);
   float y = gPad->PadtoY(yd);

   //myX = x;
   //myY = y;

   if (event==1) { // left mouse button click
           cout<<"x: "<<x<<endl;
           cout<<"y: "<<y<<endl;
      //g->SetPoint(i,x,y);
      //if (i==0) g->Draw("L");
      //i++;
      //gPad->Update();
      return;

   }
}

*/


/*
void MyMainFrame::SaveGraphs(){
TFile *f = new TFile("Graphs.root","recreate"); 
TObject *gobj, *gclone;

for(Int_t Y=0; Y<=47;Y++){
for(Int_t X=0; X<=47;X++){
TCanvas *grCanvas = (TCanvas*)fIn->Get(Form("c_Scurve_y%02d_x%02d", Y, X))->Clone();
	gCanvas = (TCanvas*)fEcan->GetCanvas();
	gCanvas->SetLogy();	
	gPad = (TPad*)gCanvas->GetPad(0);

	gPad->GetListOfPrimitives()->RemoveAll();

	TIter next(tmpCanvas->GetListOfPrimitives());
  	while ((obj=next()))
	{
     		gROOT->SetSelectedPad(gPad);
     		gclone = gobj->Clone();
     		gPad->GetListOfPrimitives()->Add(gclone,gobj->GetDrawOption());
  	}

	TH1F *hhs = (TH1F*)gCanvas->FindObject(  Form("h_Scurve_y%02d_x%02d", Y, X)     );
	Double_t noPMT =GetnPMTFromXY(Y,X);
	hhs->SetTitle(Form("h_Scurve: x=%02d y=%02d", PMT=%02d, Y, X, noPMT));
	hhs->GetXaxis()->SetRange(200, 700);
	hhs->SetMaximum(3.e2);
	hhs->SetMinimum(1.e-6);

gPad->Update();
gCanvas->Write();
}}

f->Close();	
}
*/



void MyMainFrame::ReadEstimatedDacs(TString DacFileName)
{
	FILE *fIn = fopen(DacFileName.Data(), "r");
	Char_t buf[50]; for(Int_t i=0; i<50; i++) buf[i]='\0';

	Double_t d1, d2, d3, d4, d5, d6;

		//EstimatedDacPMT[36];
	Int_t nL = 0;
	while(fgets(buf, 50, fIn))
	{
		
		sscanf(buf, "%lf %lf %lf %lf %lf %lf", &EstimatedDacPMT[nL*6], &EstimatedDacPMT[nL*6+1], &EstimatedDacPMT[nL*6+2], &EstimatedDacPMT[nL*6+3], &EstimatedDacPMT[nL*6+4], &EstimatedDacPMT[nL*6+5]);
		printf("DAC: %lf %lf %lf %lf %lf %lf\n", EstimatedDacPMT[nL*6], EstimatedDacPMT[nL*6+1], EstimatedDacPMT[nL*6+2], EstimatedDacPMT[nL*6+3], EstimatedDacPMT[nL*6+4], EstimatedDacPMT[nL*6+5]);

		nL++;
	}

	//printf("DAC: %lf %lf %lf %lf %lf %lf\n", &EstimatedDacPMT[nL*6], &EstimatedDacPMT[nL*6+1], &EstimatedDacPMT[nL*6+2], &EstimatedDacPMT[nL*6+3], &EstimatedDacPMT[nL*6+4], &EstimatedDacPMT[nL*6+5]);
	fclose(fIn);
}

Int_t MyMainFrame::GetnPMTFromXY(Int_t x, Int_t y)
{
        return (5-(y/8))*6 +(x/8)+ 1;
}

void MyMainFrame::GetXYfromPMT(Int_t nPMT, Int_t &xx, Int_t &yy)
{
Int_t xMin = 50;
Int_t yMin = 50;


        for(Int_t x=0; x<48; x++)
        for(Int_t y=0; y<48; y++)
        {
                if(GetnPMTFromXY(x,y)==nPMT )
                {
                        if(x<xMin) xMin = x;
                        if(y<yMin) yMin = y;
                }
        }

        xx = xMin;
        yy = yMin;
}
