#!/usr/bin/python
# Attempts to find pedestal start in S-Curves

import ROOT
import numpy as np
import sys
from collections import defaultdict
#from etoshelpers import arrays2graph, root_colors, wait4key

ROOT.gStyle.SetPalette(1)

# How far from pedestal to set the threshold?
thr_diff = 50

#fn = "tree.root"
fn = sys.argv[1]
f = ROOT.TFile(fn, "read")
t = f.Get("tcal")

points_x, points_y, points_z = [],[],[]

h = ROOT.TH2D("h", "h", 48, 0, 47, 48, 0, 47)
h.SetStats(0)
h_thr = ROOT.TH2D("h_thr", "h_thr", 48, 0, 47, 48, 0, 47)
h_thr.SetStats(0)
h_pmt_thr = ROOT.TH2D("h_pmt_thr", "h_pmt_thr", 6, 0, 5, 6, 0, 5)
h_pmt_thr.SetStats(0)

# Pixels thresholds
pix_thr = np.zeros((48,48))

# Pixels thresholds for each PMT
pmt_pix_thr = defaultdict(list)

# Find starts of pedestals for each pixel
for x in xrange(0, 48):
	for y in xrange(0, 48):
		t.Draw("avg_pc_data[0][0]["+str(x)+"]["+str(y)+"]:pc_threshold[0][0][0][0]", "", "goff")
		py = np.array(np.frombuffer(t.GetV1(), dtype=np.float64, count=t.GetSelectedRows()))
		px = np.array(np.frombuffer(t.GetV2(), dtype=np.float64, count=t.GetSelectedRows()))

		# pmt indices
		pmt_x = x/8
		pmt_y = y/8

		# Indices of DAC>300
		ind300 = np.argwhere(px>300)

		# Points lower by at least 1 than the next point
		grads = np.where(py[1:]-py[:-1]>1)[0]

		# Pedestal should be the first gradient >1, but about 300 (in case of some S-Curve errors)
		if np.any(py>1):
			#print x, y, np.where(py[1:]-py[:-1]>1)[0][0], points_x[-1][np.where(py[1:]-py[:-1]>1)[0][0]]
			#print x, y, grads
			# Points DAC>300, lower by at least 1 than the next point
			intersection = np.intersect1d(grads, ind300)
			if len(intersection>0):
				ind = intersection[0]
				#print px[ind], grads, ind
				h.SetBinContent(x+1,y+1,px[ind])
				pix_thr[x,y]=px[ind]-thr_diff
				h_thr.SetBinContent(x+1,y+1,pix_thr[x,y])
				pmt_pix_thr[pmt_x,pmt_y].append(pix_thr[x,y])
				continue

		# If this point reached, S-Curve was definitely bad, set threshold to 0
		h.SetBinContent(x,y,0)

# Loop through all pmts and set threshold for them
for pmt_x in xrange(0,6):
	for pmt_y in xrange(0,6):
		pmt_thr = np.percentile(pmt_pix_thr[pmt_x,pmt_y], 10, interpolation="lower")
		print pmt_x,pmt_y,pmt_thr, pmt_pix_thr[pmt_x,pmt_y]
		h_pmt_thr.SetBinContent(pmt_x+1,pmt_y+1,pmt_thr)
		
c1 = ROOT.TCanvas()
h.Draw("colz text")
c2 = ROOT.TCanvas()
h_thr.Draw("colz text")
c3 = ROOT.TCanvas()
h_pmt_thr.Draw("colz text")

rep = ''
while not rep in [ 'q', 'Q' ]:
	ROOT.gSystem.ProcessEvents()
	rep = raw_input('enter "q" to quit: ')
	if 1 < len(rep):
		rep = rep[0]
