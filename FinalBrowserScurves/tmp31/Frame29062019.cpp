#include <TGFrame.h>
#include <TGLabel.h>
#include <TGListBox.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TFile.h>
#include <TH1F.h>
#include <TText.h>
#include <TList.h>
#include <TLatex.h>
#include <TH2F.h>

#include <Riostream.h>
#include "TObject.h"
#include "TLine.h"

#include <TColor.h>
#include <TROOT.h>
#include <TGClient.h>
#include <TLegend.h>
#include <TGraph.h>
#include <TGraphErrors.h>
#include "Grafy.h"
//#include "SRC/TAmplituda.h"
//#include "SRC/TSplotX.h"
//#include "SRC/TPrzekroje.h"

#include "Exec.h"

Int_t X;
Int_t Y;
Int_t DAC;


class MyMainFrame : public TGMainFrame {
	
	private:
	TGMainFrame *fMainFrame;		// Glowna ramka (zawsze)
	TGHorizontalFrame *fHFrameCanvas;		// Pionowa ramka dla zelaza
	TGHorizontalFrame *fHFrameButtons;	// Pionowa ramka dla protonu
	
	TGGroupFrame *fGFrameCanvas, *fGFrameButtons, *fGFrameData;	// Pionowe ramki (z obramowaniem i podpisem, grupowe)

	TRootEmbeddedCanvas  *fEcan;	// Canvas zalaczony w oknie
	TCanvas *fCanvas;
	TPad *fPad;


	TGGroupFrame *fGD,  *fGDac;

	TGTextButton *fDPlus, *fDMinus, *fDacPlus, *fDacMinus;

	TGTextButton *fSzukaj;

	TGLabel *fLD, *fLDac;

	Double_t EstimatedDacPMT[36];	// Tablica wyznaczonych wartości DAC10

	TLine *lDac;

	
	public:
	MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h);
	virtual ~MyMainFrame();
	void DoExit();
	Char_t * GetCanvasName(Double_t tt, Double_t aa, Double_t gg);




	void DPlus();
	void DMinus();

	void DacPlus();
        void DacMinus();


	void DoDraw();

	void XYFromPixelNumber(Int_t n);
//	void myexec();
	void EstimatedDacs(TString DacFileName);
	Int_t GetnPMTFromXY(Int_t x, Int_t y);


	// ----------------------------------------
	//TFile *fIn = new TFile("../Frascatti/Progs_fit_JS_190525/ScurveOut.root","readonly");
	TFile *fIn = new TFile("ScurveOut.root","readonly");

	//Int_t X;// = 40;
	//Int_t Y;// = 40;


	// Tutaj ustawić startowy pixel !!!
	Int_t nPix;// = 0;
	Int_t DAC;


	TCanvas *tmpCanvas;

	TString canv_name; //= Form("c_Scurve_y%02d_x%02d", y, x);

	ClassDef(MyMainFrame,0);
};


MyMainFrame::MyMainFrame(const TGWindow *p, UInt_t w, UInt_t h) : TGMainFrame(p, w, h)
{
	EstimatedDacs("EstimatedDac10.txt");
	lDac = new TLine();
	
	lDac->SetY1(1.e6);
        lDac->SetY2(1.e-6);
	lDac->SetLineStyle(9);
	lDac->SetLineWidth(2.0);
	lDac->SetLineColor(kRed);


	DAC = 550;
	nPix = 14*48;


	// Main Frame
	fMainFrame = new TGMainFrame(0, 10, 10, kMainFrame | kVerticalFrame);
	fMainFrame->SetLayoutBroken(kTRUE);


	//__________________________________________________________________________	
	// CANVAS
	fHFrameCanvas = new TGHorizontalFrame(fMainFrame, 375, 380, kHorizontalFrame);
	fGFrameCanvas = new TGGroupFrame(fHFrameCanvas, "CANVAS");
	fGFrameCanvas->SetLayoutBroken(kTRUE);

	//____________________________________________
	// TUTAJ CIALO PROGRAMU DLA CANVAS
	fEcan = new TRootEmbeddedCanvas(0,fGFrameCanvas,500,300);
   	fGFrameCanvas->AddFrame(fEcan, new TGLayoutHints(kLHintsTop | kLHintsLeft |
                                     kLHintsExpandX  | kLHintsExpandY,0,0,1,1));
	fEcan->MoveResize(10,20,700,600);
	//fEcan->SetLogy();

	fCanvas = (TCanvas*)fEcan->GetCanvas();
        fCanvas->AddExec("myExec", "myexec(X,Y)");
	//fCanvas->AddExec("myExec","MyMainFrame::myexec()");

	//_______________________________________________________________________

	fHFrameCanvas->AddFrame(fGFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fGFrameCanvas->MoveResize(5,5, 720, 640);
	fMainFrame->AddFrame(fHFrameCanvas, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fHFrameCanvas->MoveResize(10,10,730 ,650);
	// END OF CANVAS
	//_____________________________________________________________________________________



	//___________________________________________________________________________
	//BUTTONS
	fHFrameButtons = new TGHorizontalFrame(fMainFrame, 175, 380, kHorizontalFrame);
        fGFrameButtons = new TGGroupFrame(fHFrameButtons, "PARAMETERS");
        fGFrameButtons->SetLayoutBroken(kTRUE);

	//________________________________________________ 
	// TUTAJ CIALO PROGRAMU DLA BUTTONS

	// **************** DAC ***************************
        fGDac = new TGGroupFrame(fGFrameButtons, "DAC:");
        fGDac->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- DAC ---------------------
        fDacPlus = new TGTextButton(fGDac, "&PLUS");
        fDacPlus->Connect("Pressed()", "MyMainFrame", this, "DacPlus()");
        fGDac->AddFrame(fDacPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDacPlus->MoveResize(10, 15, 60,30);

        fDacMinus = new TGTextButton(fGDac, "&MINUS");
        fDacMinus->Connect("Pressed()", "MyMainFrame", this, "DacMinus()");
        fGDac->AddFrame(fDacMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDacMinus->MoveResize(80, 15, 60,30);
        // ----------------------------------------------------------------

        fLDac = new TGLabel(fGDac, "0.73 ");
        fGDac->AddFrame(fLDac, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLDac->MoveResize(10, 55, 140, 20);


        fGFrameButtons->AddFrame(fGDac, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGDac->MoveResize(5,140,155, 90);
        // *********************




	// **************** D ***************************
        fGD = new TGGroupFrame(fGFrameButtons, "DATA:");
        fGD->SetLayoutBroken(kTRUE);

        // --- Przyciski +/- D 
        fDPlus = new TGTextButton(fGD, "&PLUS");
        fDPlus->Connect("Pressed()", "MyMainFrame", this, "DPlus()");
        fGD->AddFrame(fDPlus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDPlus->MoveResize(10, 15, 60,30);

        fDMinus = new TGTextButton(fGD, "&MINUS");
        fDMinus->Connect("Pressed()", "MyMainFrame", this, "DMinus()");
        fGD->AddFrame(fDMinus, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fDMinus->MoveResize(80, 15, 60,30);
        // -----------------------------------------------------------------

	fGD->SetBackgroundColor(gROOT->GetColor(kGray)->GetPixel());
	fLD = new TGLabel(fGD, "1960 GeV");
        fGD->AddFrame(fLD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fLD->MoveResize(10, 55, 140, 20);


	fSzukaj = new TGTextButton(fGD, "&DoDraw");
	fSzukaj->Connect("Pressed()", "MyMainFrame", this, "DoDraw()");
	fGD->AddFrame(fSzukaj, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
	fSzukaj->MoveResize(10, 85, 140, 20);


        fGFrameButtons->AddFrame(fGD, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGD->MoveResize(5,15,155, 120);
        // *********************


	//________________________________________________________________________________________

        fHFrameButtons->AddFrame(fGFrameButtons, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fGFrameButtons->MoveResize(5,15, 165, 600);



        fMainFrame->AddFrame(fHFrameButtons, new TGLayoutHints(kLHintsLeft | kLHintsTop, 2,2,2,2));
        fHFrameButtons->MoveResize(740,10,200 ,640);	
	//END OF BUTTONS
	//___________________________________________________________________________________________


	// Configuration of main frame
	fMainFrame->SetMWMHints(kMWMDecorAll, kMWMFuncAll, kMWMInputModeless);
	fMainFrame->MapSubwindows();
	fMainFrame->SetWindowName("Moje Nowe Okno");
	fMainFrame->Resize(fMainFrame->GetDefaultSize());
	fMainFrame->MapWindow();
	fMainFrame->Resize(w, h);
	fMainFrame->Layout();

	DoDraw();

}	

MyMainFrame::~MyMainFrame()
{
	Cleanup();
}


void MyMainFrame::DoExit()
{
	gApplication->Terminate(0);
}





void MyMainFrame::DPlus()
{
	nPix = nPix +1;
	if(nPix>2303) nPix=2303;
	//cout<<"nPix: "<<nPix<<endl;
        DoDraw();
}

void MyMainFrame::DMinus()
{
	nPix = nPix - 1;
	if(nPix<0) nPix=0;
	//cout<<"nPix: "<<nPix<<endl;
        DoDraw();
}


void MyMainFrame::DacPlus()
{
	DAC = DAC + 1;
        //nPix = nPix +1;
        if(DAC>1023) nPix=1023;
        //cout<<"nPix: "<<nPix<<endl;
        DoDraw();
}

void MyMainFrame::DacMinus()
{
	DAC = DAC - 1;
        //nPix = nPix - 1;
        if(DAC<0) DAC=0;
        //cout<<"nPix: "<<nPix<<endl;
        DoDraw();
}




// funkcja zwraca ciag bedacy nazwa canvasu na podstawie podanych parametrow x y z t a g
Char_t * MyMainFrame::GetCanvasName(Double_t tt, Double_t aa, Double_t gg)
{
	
	Char_t *FF = new Char_t[200]; for(Int_t i=0; i<200; i++) FF[i] = '\0';
	sprintf(FF, "ROOT/WIDC2%.02fAVCH0%.02fTRAP%.02f.root", gg,aa,tt);


        //if(gg<0) sprintf(FF, "Theta_%02d__Phi_%03d__GTU_%03d", tt, aa, gg);
        //else sprintf(FF, "Theta_%02d__Phi_%03d__GTU_+%02d", tt, aa, gg);
        return FF;
}



/*
TGraph * MyMainFrame::ChiSquare(TGraphErrors *dane, TGraph *suma, Double_t  &ChOut)
{
        Double_t Chi2 = 0.0;
        Double_t x = 0.0;
        Double_t y = 0.0;

	TGraph *grchi = new TGraph();

        Int_t nPoints = dane->GetN();
	Double_t ErrorY;

        for(Int_t i=0; i<nPoints; i++)
        {
                dane->GetPoint(i, x, y);
		Double_t SumaY = suma->Eval(x);
		Double_t Diff = y - SumaY;
		ErrorY = dane->GetErrorY(i);
		Double_t Chi2ThisPoint = ((Diff*Diff)/(ErrorY*ErrorY));
		grchi->SetPoint(i, x, Chi2ThisPoint);
		Chi2 = Chi2 + Chi2ThisPoint;	
        }
	grchi->SetMarkerStyle(28);
	grchi->SetMarkerColor(kRed);
	grchi->GetXaxis()->SetTitle("Position of point on |t| axis");
	grchi->GetYaxis()->SetTitle("#Chi^{2} ");

	ChiSquareMin = Chi2;
	ChOut = Chi2;
        printf("CHI2: %f\n", Chi2);
	return grchi;
}
*/


/*
Double_t MyMainFrame::ChiSquare(TGraphErrors *dane, TGraph *suma)
{
        Double_t Chi2 = 0.0; Double_t x = 0.0; Double_t y = 0.0;
        TGraph *grchi = new TGraph();
        Int_t nPoints = dane->GetN();
        Double_t ErrorY;

        for(Int_t i=0; i<nPoints; i++)
        {
                dane->GetPoint(i, x, y);
                Double_t SumaY = suma->Eval(x);
                Double_t Diff = y - SumaY;
                ErrorY = dane->GetErrorY(i);
                Double_t Chi2ThisPoint = ((Diff*Diff)/(ErrorY*ErrorY));
                grchi->SetPoint(i, x, Chi2ThisPoint);
                Chi2 = Chi2 + Chi2ThisPoint;
        }
        grchi->SetMarkerStyle(28);
        grchi->SetMarkerColor(kRed);
        grchi->GetXaxis()->SetTitle("Position of point on |t| axis");
        grchi->GetYaxis()->SetTitle("#Chi^{2} ");

        //Double_t DChi2 = Chi2;
        return Chi2;
}
*/



void MyMainFrame::DoDraw()
{
	//cout<<"FajnaFunkcja"<<endl;

	TObject *obj, *clone;
	fLD->SetText(Form("%d", nPix));
        fLD->Layout();
	fLDac->SetText(Form("%d", DAC));
        fLDac->Layout();


	XYFromPixelNumber(nPix);



	TCanvas *tmpCanvas = (TCanvas*)fIn->Get(Form("c_Scurve_y%02d_x%02d", Y, X))->Clone();
	fCanvas = (TCanvas*)fEcan->GetCanvas();	
	fPad = (TPad*)fCanvas->GetPad(0);

	fPad->GetListOfPrimitives()->RemoveAll();

	TIter next(tmpCanvas->GetListOfPrimitives());
  	while ((obj=next()))
	{
     		gROOT->SetSelectedPad(fPad);
     		clone = obj->Clone();
     		fPad->GetListOfPrimitives()->Add(clone,obj->GetDrawOption());
  	}

	TH1F *hh = (TH1F*)fCanvas->FindObject(  Form("h_Scurve_y%02d_x%02d", Y, X)     );
	hh->GetXaxis()->SetRange(200, 700);

	cout<<"Dac10: "<<EstimatedDacPMT[GetnPMTFromXY(Y,X)-1]<<endl;
	cout<<"X: "<<X<<" Y: "<<Y<<endl;
	cout<<"nPMT: "<<GetnPMTFromXY(Y,X)<<endl;


	lDac->SetX1(1008 - EstimatedDacPMT[GetnPMTFromXY(Y,X)-1]);
	lDac->SetX2(1008 - EstimatedDacPMT[GetnPMTFromXY(Y,X)-1]);

	lDac->SetY1(hh->GetMaximum());
	lDac->SetY2(hh->GetMinimum());


	//tmpCanvas = (TCanvas*)fIn->Get(Form("c_Scurve_y%02d_x%02d", Y, X))->Clone();
	fCanvas->SetLogy();
	fCanvas->Draw();
	lDac->Draw("same");
	fCanvas->Update();

/*	
	printf("M: %f, K: %f, R: %f, A: %f, L: %f\n", M, K, R, A, L);
	printf("%s\n", NameAmplRe(M, K, A, R,1.0, 0.0, L));
	printf("%s\n", NameAmplIm(M, K, A, R, 1.0, 0.0,L));
	printf("%s\n", NameAmplSum(M, K, A, R, 1.0, 0.0,L));
	//return;


	Data->GetXaxis()->SetTitle("|t| [GeV]");
	Data->GetYaxis()->SetTitle("#frac{d#sigma}{d|t|} #left[ #frac{mb}{GeV} #right]");

	Double_t MH3, KB3, SX, SY;
	SX = 1.0; SY = 1.0; KB3 = 0.0; MH3 = 1.0;

	Double_t iM = M;
	Double_t iR = R;
	Double_t iK = K;
	Double_t iA = A;
	Double_t iL = L;


	system((Form("rm logs/log%03d.txt",nr)));
	
	FILE *fIn = fopen(Form("inputs/input%03d.txt",nr), "r");
        Float_t iM, iK, iR, iA, iL;
        Char_t bufIn[100]; for(Int_t i=0; i<100; i++) bufIn[i] = '\0';
        fgets(bufIn, 100, fIn);
        sscanf(bufIn, "%f %f %f %f %f", &iM, &iK, &iR, &iA, &iL);
        fclose(fIn);


	TSplotX *Splot;
	TFile *fOmega;
	TFile *fAmplituda;
	TPrzekroje *Prz;
	TGraph *grRe;
	TGraph *grIm;
	TGraph *grSum;
	TGraph *grOmega;
	TAmplituda *Amp;	


	//TThread::Lock();
	fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "readonly");
        grRe = (TGraph*)fAmplituda->Get(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
        grIm = (TGraph*)fAmplituda->Get(NameAmplIm(iM, iK, iA, iR,MH3, KB3,  iL));
        grSum = (TGraph*)fAmplituda->Get(NameAmplSum(iM, iK, iA, iR, MH3, KB3, iL));
        fAmplituda->Close();
	//TThread::UnLock();

        Data->SetTitle(Form("%s", NameAmplSum(iM, iK, iA, iR, MH3, KB3, iL)));

        if(grRe && grIm && grSum)
        {
		//TThread::Lock();
		fOmega = new TFile(Form("DBOmega/DBOmega.root"), "readonly");
                grOmega = (TGraph*)fOmega->Get(NameOmega(iM, iK, iA, iR, MH3, KB3));
                fOmega->Close();
		//TThread::UnLock();


		Prz = new TPrzekroje(grOmega);
	        //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
		Double_t DElast = Prz->PodajElastic(iL);
		Double_t DInelast = Prz->PodajInelastic(iL);
	        delete Prz; Prz = NULL;

	        //cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
		Double_t DChi2; 
		TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);
	        //cout<<Form("PROC%03d finished",nr)<<endl;
		TCanvas *cc = fEcan->GetCanvas();
                cc->cd();
		cc->SetLogy();	
		Data->Draw("AP");
		grSum->Draw("L same");		

		TLatex lt;
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
                lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


		cc->Update();

		TCanvas *cChi =  fEcanChi2->GetCanvas();
                cChi->cd();
                grChi2->Draw("AP");
                cChi->Update();

		printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));

	        return;
        } else

	{
		//TThread::Lock();		
		//fOmega = new TFile(Form("DBOmega/DBOmega%02d.root",nr), "readonly");
		fOmega = new TFile(Form("DBOmega/DBOmega.root"), "readonly");
                grOmega = (TGraph*)fOmega->Get(NameOmega(iM, iK, iA, iR, MH3, KB3));
                fOmega->Close();
		//TThread::UnLock();

		if(grOmega)
                {
                        Amp = new TAmplituda(grOmega);
                        grRe = Amp->PodajAmplitudeRe(iL);
                        grIm = Amp->PodajAmplitudeIm(iL);

                        grSum = SumaAmpl(grRe, grIm);
                        grSum->SetLineColor(kRed);
                        grRe->SetLineColor(kBlue);
                        grIm->SetLineColor(kGreen);

                        grRe->SetName(NameAmplRe(iM, iK, iA, iR,MH3, KB3, iL));
                        grRe->SetTitle(NameAmplRe(iM, iK, iA, iR,MH3, KB3, iL));
                        grIm->SetName(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
                        grIm->SetTitle(NameAmplIm(iM, iK, iA, iR,MH3, KB3,iL));
                        grSum->SetName(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
                        grSum->SetTitle(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));

			//TThread::Lock();
                        //fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda%02d.root",nr), "update");
			fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "update");
                        grRe->Write();
                        grIm->Write();
                        grSum->Write();
                        fAmplituda->Close();
			//TThread::UnLock();


			//Prz = new TPrzekroje(grOmega);
	                //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
        	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	//delete Prz; Prz = NULL;

	                //cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
        	        //cout<<Form("PROC%02d stopped",nr)<<endl;

			Prz = new TPrzekroje(grOmega);
	                //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
        	        //cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	Double_t DElast = Prz->PodajElastic(iL);
                	Double_t DInelast = Prz->PodajInelastic(iL);
                	delete Prz; Prz = NULL;

                	//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
                	//Double_t DChi2 = ChiSquare(Data, grSum);
			Double_t DChi2; 
                	TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);
                	//cout<<Form("PROC%03d finished",nr)<<endl;


                	FILE *fd = fopen(Form("logs/log%03d.txt",nr), "a+");
                	fprintf(fd, "M= %.03f K= %.03f R= %.03f A= %.03f KB3: %f, MH3: %f\n", iM, iK, iR, iA, KB3, MH3);
                	fprintf(fd, "Chi: %f El: %f Inel: %f Total: %f Ratio: %f REIM: %f\n", DChi2, DElast, DInelast, DElast+DInelast, DElast/(DElast+DInelast), (TMath::Sqrt(grRe->Eval(0.001))/TMath::Sqrt(grIm->Eval(0.001))));
                	fprintf(fd, "END\n");
                	fclose(fd);

			TCanvas *cc = fEcan->GetCanvas();
	                cc->cd();
        	        cc->SetLogy();
                	Data->Draw("AP");
                	grSum->Draw("L same");
			
			TLatex lt;
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
       			lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
        		lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


			

			cc->Update();

			TCanvas *cChi =  fEcanChi2->GetCanvas();
                        cChi->cd();
                        grChi2->Draw("AP");
                        cChi->Update();

			//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
			printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));
                	return;
		} else

		{

			Splot = new TSplotX();
        		grOmega = Splot->PodajCalke(iK, iM, iA, iR, KB3, MH3, SX, SY);
	        	delete Splot; Splot = NULL;

	        	grOmega->SetName(NameOmega(iM, iK, iA, iR, MH3, KB3));
		        grOmega->SetTitle(NameOmega(iM, iK, iA, iR, MH3, KB3));

			//TThread::Lock();
	        	//fOmega = new TFile(Form("DBOmega/DBOmega%02d.root",nr), "update");
			fOmega = new TFile(Form("DBOmega/DBOmega.root"), "update");
		        grOmega->Write();
        		fOmega->Close();
			//TThread::UnLock();

		        Amp = new TAmplituda(grOmega);
        		grRe = Amp->PodajAmplitudeRe(iL);
	        	grIm = Amp->PodajAmplitudeIm(iL);
	        	delete Amp; Amp = NULL;
	
		        grSum = SumaAmpl(grRe, grIm);
	
			grSum->SetLineColor(kRed);
		        grRe->SetLineColor(kBlue);
		        grIm->SetLineColor(kGreen);

		        grRe->SetName(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
		        grRe->SetTitle(NameAmplRe(iM, iK, iA, iR, MH3, KB3, iL));
	        	grIm->SetName(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
		        grIm->SetTitle(NameAmplIm(iM, iK, iA, iR,MH3, KB3, iL));
		        grSum->SetName(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
		        grSum->SetTitle(NameAmplSum(iM, iK, iA, iR,MH3, KB3, iL));
	
			//TThread::Lock();
			//fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda%02d.root",nr), "update");
			fAmplituda = new TFile(Form("DBAmplituda/DBAmplituda.root"), "update");
	        	grRe->Write();
		        grIm->Write();
		        grSum->Write();
	        	fAmplituda->Close();
			//TThread::UnLock();


			//Prz = new TPrzekroje(grOmega);
		        //cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
	        	//cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
		        //delete Prz; Prz = NULL;

			//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;	
			//cout<<Form("PROC%02d stopped",nr)<<endl;	

			Prz = new TPrzekroje(grOmega);
                	//cout<<"EL: "<<Prz->PodajElastic(iL)<<endl;
                	//cout<<"Inel: "<<Prz->PodajInelastic(iL)<<endl;
                	Double_t DElast = Prz->PodajElastic(iL);
                	Double_t DInelast = Prz->PodajInelastic(iL);
                	delete Prz; Prz = NULL;

                	//cout<<"CHI2: "<<ChiSquare(Data, grSum)<<endl;
                	//Double_t DChi2 = ChiSquare(Data, grSum);
                	//cout<<Form("PROC%03d finished",nr)<<endl;
			Double_t DChi2; 
                	TGraph *grChi2 = ChiSquare(Data, grSum, DChi2);


                	FILE *fd = fopen(Form("logs/log%03d.txt",nr), "a+");
                	fprintf(fd, "M= %.03f K= %.03f R= %.03f A= %.03f KB3: %f, MH3: %f\n", iM, iK, iR, iA, KB3, MH3);
                	fprintf(fd, "Chi: %f El: %f Inel: %f Total: %f Ratio: %f REIM: %f\n", DChi2, DElast, DInelast, DElast+DInelast, DElast/(DElast+DInelast), (TMath::Sqrt(grRe->Eval(0.001))/TMath::Sqrt(grIm->Eval(0.001))));
                	fprintf(fd, "END\n");
                	fclose(fd);

			TCanvas *cc = fEcan->GetCanvas();
        	        cc->cd();
	                cc->SetLogy();
                	Data->Draw("AP");
                	grSum->Draw("L same");

			TLatex lt;
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.1*Data->GetYaxis()->GetXmax(), Form("Elast: %.03e mb", DElast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.01*Data->GetYaxis()->GetXmax(), Form("Inelast: %.03e mb", DInelast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.0001*Data->GetYaxis()->GetXmax(), Form("Total: %.03e mb", DElast+DInelast));
                        lt.DrawLatex(0.60*Data->GetXaxis()->GetXmax(), 2.*0.00001*Data->GetYaxis()->GetXmax(), Form("#chi^{2}: %.03e ", DChi2));


			cc->Update();
		
			TCanvas *cChi =  fEcanChi2->GetCanvas();
			cChi->cd();
			grChi2->Draw("AP");
			//grOmega->SetMarkerStyle(28);
			//grOmega->Draw("AP");
			cChi->Update();

			//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
			printf("REIM: %.03f\n",TMath::Sqrt(grRe->Eval(0.001)/grIm->Eval(0.001)));
	
			return;
		}
	}


	//printf("Re: %.02f Im: %.02f\n",grRe->Eval(0.001), grIm->Eval(0.001));
*/


}


void MyMainFrame::XYFromPixelNumber(Int_t n)
{
	for(Int_t x=0; x<48; x++)
		for(Int_t y=0; y<48; y++)
			if( (x*48+y)==n)
			{
				X = x;
				Y = y;
				//cout<<"n: "<<n<<endl;
				//cout<<"X: "<<X<<endl;
				//cout<<"Y: "<<Y<<endl;
				return;
			}
	cout<<"Cannot determine X Y coordinates !!!!"<<endl;
	return;
}

/*
void MyMainFrame::myexec()
{
   // get event information
   int event = gPad->GetEvent();
   //myEvent = event;
   int px    = gPad->GetEventX();
   int py    = gPad->GetEventY();

   //if(event==1) gApplication->Terminate(0);
   // some magic to get the coordinates...
   double xd = gPad->AbsPixeltoX(px);
   double yd = gPad->AbsPixeltoY(py);
   float x = gPad->PadtoX(xd);
   float y = gPad->PadtoY(yd);

   //myX = x;
   //myY = y;

   if (event==1) { // left mouse button click
           cout<<"x: "<<x<<endl;
           cout<<"y: "<<y<<endl;
      //g->SetPoint(i,x,y);
      //if (i==0) g->Draw("L");
      //i++;
      //gPad->Update();
      return;

   }
}

*/


void MyMainFrame::EstimatedDacs(TString DacFileName)
{
	FILE *fIn = fopen(DacFileName.Data(), "r");
	Char_t buf[50]; for(Int_t i=0; i<50; i++) buf[i]='\0';

	Double_t d1, d2, d3, d4, d5, d6;

		//EstimatedDacPMT[36];
	Int_t nL = 0;
	while(fgets(buf, 50, fIn))
	{
		
		sscanf(buf, "%lf %lf %lf %lf %lf %lf", &EstimatedDacPMT[nL*6], &EstimatedDacPMT[nL*6+1], &EstimatedDacPMT[nL*6+2], &EstimatedDacPMT[nL*6+3], &EstimatedDacPMT[nL*6+4], &EstimatedDacPMT[nL*6+5]);
		nL++;
	}

	fclose(fIn);
}

Int_t MyMainFrame::GetnPMTFromXY(Int_t x, Int_t y)
{
        return (5-(y/8))*6 +(x/8)+ 1;
}


